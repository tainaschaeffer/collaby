SELECT "Grupo" AS "grupo",
       "Aluno" AS "aluno",
       "Papel no grupo",
       "Nota inicial",
       "Nota final",
       "Intervenções",
       "Intervenções relevantes",
       "Intervenções solícitas",
       "Tamanho médio mensagens do aluno", 
       round((
         SELECT SUM(ROUND(COALESCE("media", 0), 2)) / COUNT(*)
            FROM 
          (SELECT 
                round((SELECT avg(process_data.count_chars) 
                    FROM process_data
                  WHERE process_data.class_id = :class_id
                    AND process_data.group_id = groups.id
                    AND process_data.date_process = :date_process
                    AND process_data.student_id = student_class.student_id), 2) AS "media",
                coalesce(((SELECT count(DISTINCT message.id) 
                    FROM message 
                  WHERE message.group_id = groups.id 
                    AND message.student_id = student_class.student_id) +
                (SELECT count(DISTINCT answer.id) 
                    FROM answer, message 
                  WHERE answer.message_id = message.id 
                    AND message.group_id = groups.id 
                    AND answer.student_id = student_class.student_id)), 0)::decimal AS "Intervenções",
                (
                    SELECT count(*)
                      FROM process_data solicitude_process_data
                      WHERE solicitude_process_data.class_id = :class_id
                        AND solicitude_process_data.group_id = groups.id
                        AND solicitude_process_data.date_process = :date_process
                        AND solicitude_process_data.prob_1::float >= :solicitude_message_percentage
                        AND solicitude_process_data.student_id = student_class.student_id
                )::decimal AS "Intervenções solícitas"
            FROM class, 
                student_class, 
                groups,
                student_group,
                system_users
          WHERE student_class.class_id = class.id
            AND groups.class_id = class.id
            AND groups.id = student_group.group_id
            AND student_class.student_id = student_group.student_id
            AND student_class.student_id = system_users.id
            AND class.id = :class_id
            ) AS tabela
          WHERE (CASE WHEN (CASE WHEN "Intervenções" > 0 THEN ("Intervenções solícitas" / "Intervenções") ELSE 0 END)> :solicitude_message_percentage THEN 1 ELSE 0 END) = 0

       ), 2) AS "Tamanho médio mensagens dos alunos não solícitos",
       "Tamanho médio mensagens relevantes",
       "Quantidade de perguntas do grupo",
       "Quantidade de perguntas respondidas",
       "Índice de participação",
       "% de mensagens classificadas solícitas pelo minerador",
       "% de mensagens consideradas relevantes",
       "Observável S" AS "observavel_s",
       (CASE WHEN "Índice de participação" >= :participation_percentage AND "Tamanho médio mensagens do aluno" > (round((
         SELECT SUM(ROUND(COALESCE("media", 0), 2)) / COUNT(*)
            FROM 
          (SELECT 
                round((SELECT avg(process_data.count_chars) 
                    FROM process_data
                  WHERE process_data.class_id = :class_id
                    AND process_data.group_id = groups.id
                    AND process_data.date_process = :date_process
                    AND process_data.student_id = student_class.student_id), 2) AS "media",
                coalesce(((SELECT count(DISTINCT message.id) 
                    FROM message 
                  WHERE message.group_id = groups.id 
                    AND message.student_id = student_class.student_id) +
                (SELECT count(DISTINCT answer.id) 
                    FROM answer, message 
                  WHERE answer.message_id = message.id 
                    AND message.group_id = groups.id 
                    AND answer.student_id = student_class.student_id)), 0)::decimal AS "Intervenções",
                (
                    SELECT count(*)
                      FROM process_data solicitude_process_data
                      WHERE solicitude_process_data.class_id = :class_id
                        AND solicitude_process_data.group_id = groups.id
                        AND solicitude_process_data.date_process = :date_process
                        AND solicitude_process_data.prob_1::float >= :solicitude_message_percentage
                        AND solicitude_process_data.student_id = student_class.student_id
                )::decimal AS "Intervenções solícitas"
            FROM class, 
                student_class, 
                groups,
                student_group,
                system_users
          WHERE student_class.class_id = class.id
            AND groups.class_id = class.id
            AND groups.id = student_group.group_id
            AND student_class.student_id = student_group.student_id
            AND student_class.student_id = system_users.id
            AND class.id = :class_id
            ) AS tabela
          WHERE (CASE WHEN (CASE WHEN "Intervenções" > 0 THEN ("Intervenções solícitas" / "Intervenções") ELSE 0 END)> :solicitude_message_percentage THEN 1 ELSE 0 END) = 0

       ), 2) * 1.5) THEN 1 ELSE 0 END) AS "observavel_p",
       "Observável R" AS "observavel_r",
       (CASE WHEN "Observável S" = 1 AND (CASE WHEN "Índice de participação" >= :participation_percentage AND "Tamanho médio mensagens do aluno" > (round((
         SELECT SUM(ROUND(COALESCE("media", 0), 2)) / COUNT(*)
            FROM 
          (SELECT 
                round((SELECT avg(process_data.count_chars) 
                    FROM process_data
                  WHERE process_data.class_id = :class_id
                    AND process_data.group_id = groups.id
                    AND process_data.date_process = :date_process
                    AND process_data.student_id = student_class.student_id), 2) AS "media",
                coalesce(((SELECT count(DISTINCT message.id) 
                    FROM message 
                  WHERE message.group_id = groups.id 
                    AND message.student_id = student_class.student_id) +
                (SELECT count(DISTINCT answer.id) 
                    FROM answer, message 
                  WHERE answer.message_id = message.id 
                    AND message.group_id = groups.id 
                    AND answer.student_id = student_class.student_id)), 0)::decimal AS "Intervenções",
                (
                    SELECT count(*)
                      FROM process_data solicitude_process_data
                      WHERE solicitude_process_data.class_id = :class_id
                        AND solicitude_process_data.group_id = groups.id
                        AND solicitude_process_data.date_process = :date_process
                        AND solicitude_process_data.prob_1::float >= :solicitude_message_percentage
                        AND solicitude_process_data.student_id = student_class.student_id
                )::decimal AS "Intervenções solícitas"
            FROM class, 
                student_class, 
                groups,
                student_group,
                system_users
          WHERE student_class.class_id = class.id
            AND groups.class_id = class.id
            AND groups.id = student_group.group_id
            AND student_class.student_id = student_group.student_id
            AND student_class.student_id = system_users.id
            AND class.id = :class_id
            ) AS tabela
          WHERE (CASE WHEN (CASE WHEN "Intervenções" > 0 THEN ("Intervenções solícitas" / "Intervenções") ELSE 0 END)> :solicitude_message_percentage THEN 1 ELSE 0 END) = 0

       ), 2) * 1.5) THEN 1 ELSE 0 END) = 1 AND "Observável R" = 1 THEN 1 ELSE 0 END) AS "solicito_ativo",
       "Id aluno"
FROM
(SELECT "Id aluno",
       "Aluno",
       "Grupo",
       "Papel no grupo",
       "Nota inicial",
       "Nota final",
       "Intervenções",
       "Intervenções relevantes",
       "Intervenções solícitas",
       COALESCE("Tamanho médio mensagens do aluno", 0) AS "Tamanho médio mensagens do aluno", 
       "Tamanho médio mensagens dos alunos não solícitos",
       "Tamanho médio mensagens relevantes",
       "Quantidade de perguntas do grupo",
       "Quantidade de perguntas respondidas",
       "Índice de participação",
       "% de mensagens classificadas solícitas pelo minerador",
       "% de mensagens consideradas relevantes",
       (CASE WHEN "% de mensagens classificadas solícitas pelo minerador" > :solicitude_message_percentage THEN 1 ELSE 0 END) AS "Observável S",
       --(CASE WHEN "Índice de participação" >= :participation_percentage AND "Tamanho médio mensagens do aluno" > ("Tamanho médio mensagens dos alunos não solícitos" * 1.5) THEN 1 ELSE 0 END) AS "Observável P",
       (CASE WHEN "% de mensagens consideradas relevantes" >= :relevance_percentage THEN 1 ELSE 0 END) AS "Observável R"
FROM 
(SELECT "Id aluno",
       "Aluno",
       "Grupo",
       "Papel no grupo",
       "Nota inicial",
       "Nota final",
       "Intervenções",
       "Intervenções relevantes",
       "Intervenções solícitas",
       "Tamanho médio mensagens do aluno", 
       "Tamanho médio mensagens dos alunos não solícitos",
       "Tamanho médio mensagens relevantes",
       "Quantidade de perguntas do grupo",
       "Quantidade de perguntas respondidas",
       round((CASE WHEN "Quantidade de perguntas do grupo" > 0 THEN ("Quantidade de perguntas respondidas" / "Quantidade de perguntas do grupo") ELSE 0 END), 2) AS "Índice de participação",
       round((CASE WHEN "Intervenções" > 0 THEN ("Intervenções solícitas" / "Intervenções") ELSE 0 END), 2) AS "% de mensagens classificadas solícitas pelo minerador",
       round((CASE WHEN "Intervenções" > 0 THEN ("Intervenções relevantes" / "Intervenções") ELSE 0 END), 2) AS "% de mensagens consideradas relevantes"
  FROM 
(SELECT groups.name AS "Grupo",
       system_users.name AS "Aluno",
       student_class.role AS "Papel no grupo",
       student_class.grade_ini AS "Nota inicial", 
       student_class.grade_end AS "Nota final",
       coalesce(((SELECT count(message.id) 
          FROM message 
         WHERE message.group_id = groups.id 
           AND message.student_id = student_class.student_id
           AND message.rating NOT IN ('irrelevante')) +
       (SELECT count(answer.id) 
          FROM answer, message 
         WHERE answer.message_id = message.id 
           AND message.group_id = groups.id 
           AND answer.student_id = student_class.student_id
           AND answer.rating NOT IN ('irrelevante'))), 0)::decimal AS "Intervenções",
       coalesce(((SELECT count(message.id) 
          FROM message 
         WHERE message.group_id = groups.id 
           AND message.student_id = student_class.student_id
           AND message.rating IN ('relevante', 'muito_relevante')) +
       (SELECT count(answer.id) 
          FROM answer, message 
         WHERE answer.message_id = message.id 
           AND message.group_id = groups.id 
           AND answer.student_id = student_class.student_id
           AND answer.rating IN ('relevante', 'muito_relevante'))), 0)::decimal AS "Intervenções relevantes",
       (
           SELECT count(distinct description)
             FROM process_data solicitude_process_data
            WHERE solicitude_process_data.class_id = :class_id
              AND solicitude_process_data.group_id = groups.id
              AND solicitude_process_data.date_process = :date_process
              AND solicitude_process_data.prob_1::float >= :solicitude_message_percentage
              AND solicitude_process_data.student_id = student_class.student_id
       )::decimal AS "Intervenções solícitas",
       round((SELECT avg(process_data.count_chars) 
          FROM process_data
         WHERE process_data.class_id = :class_id
           AND process_data.group_id = groups.id
           AND process_data.date_process = :date_process
           AND process_data.student_id = student_class.student_id), 2) AS "Tamanho médio mensagens do aluno",
        round((SELECT avg(process_data.count_chars) 
          FROM message,
               process_data
         WHERE process_data.description_id = message.id 
           AND process_data.prob_1::float < :solicitude_message_percentage
           AND process_data.class_id = :class_id
           AND process_data.date_process = :date_process
           AND message.rating NOT IN ('irrelevante')), 2) AS "Tamanho médio mensagens dos alunos não solícitos",
       (SELECT avg(process_data.count_chars) 
          FROM message,
               process_data
         WHERE message.group_id = groups.id 
           AND process_data.description_id = message.id
           AND message.student_id = student_class.student_id
           AND process_data.class_id = :class_id
           AND process_data.group_id = groups.id
           AND process_data.date_process = :date_process
           AND process_data.student_id = student_class.student_id
           AND message.rating IN ('relevante', 'muito_relevante')) AS "Tamanho médio mensagens relevantes",
       (SELECT count(message.id) 
          FROM message 
         WHERE message.group_id = groups.id
           AND message.rating NOT IN ('irrelevante'))::decimal AS "Quantidade de perguntas do grupo",
       (SELECT count(DISTINCT message.id) 
          FROM answer, message 
         WHERE answer.message_id = message.id 
           AND message.group_id = groups.id 
           AND answer.student_id = student_class.student_id
           AND answer.rating NOT IN ('irrelevante'))::decimal AS "Quantidade de perguntas respondidas",
        system_users.id AS "Id aluno"
  FROM class, 
       student_class, 
       groups,
       student_group,
       system_users
 WHERE student_class.class_id = class.id
   AND groups.class_id = class.id
   AND groups.id = student_group.group_id
   AND student_class.student_id = student_group.student_id
   AND student_class.student_id = system_users.id
   AND class.id = :class_id
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 14, groups.id, student_class.student_id
ORDER BY 1, 2) AS tabela) AS tabela1) AS tabela2