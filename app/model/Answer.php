<?php

class Answer extends TRecord
{
    const TABLENAME  = 'answer';
    const PRIMARYKEY = 'id';
    const IDPOLICY   =  'serial'; // {max, serial}

    private $message;
    private $student;

    const RELEVANT     = 2;
    const VERYRELEVANT = 3;
    
    const RATING = [4 => 'irrelevante',
                    1 => 'conversa_social',
                    2 => 'relevante',
                    3 => 'muito_relevante'];

    const RATING_ICON = [4 => 'trash',
                         1 => 'comments',
                         2 => 'thumbs-up',
                         3 => 'star'];

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('message_id');
        parent::addAttribute('answer');
        parent::addAttribute('student_id');
        parent::addAttribute('date_answer');
        parent::addAttribute('rating');
        parent::addAttribute('attachment');
        parent::addAttribute('count_chars');
    }

    public function get_student_name()
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('id', '=', $this->student_id));

        $system_user = SystemUsers::getObjects($criteria);
        return $system_user[0]->name;
    }

    /**
     * Method set_message
     * Sample of usage: $var->message = $object;
     * @param $object Instance of Class
     */
    public function set_message(Message $object)
    {
        $this->message = $object;
        $this->message_id = $object->id;
    }

    /**
     * Method get_message
     * Sample of usage: $var->message->attribute;
     * @returns Class instance
     */
    public function get_message()
    {
    
        // loads the associated object
        if (empty($this->message))
            $this->message = new Message($this->message_id);
    
        // returns the associated object
        return $this->message;
    }

    public function set_student(SystemUsers $object)
    {
        $this->student = $object;
        $this->student_id = $object->id;
    }

    public function get_student()
    {
    
        // loads the associated object
        if (empty($this->student))
            $this->student = new SystemUsers($this->student_id);
    
        // returns the associated object
        return $this->student;
    }   
}

