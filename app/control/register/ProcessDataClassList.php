<?php

class ProcessDataClassList extends TPage
{
    private $form; // form
    private $datagrid; // listing
    private $pageNavigation;
    private $loaded;
    private $filter_criteria;
    private static $database = 'collaby';
    private static $activeRecord = 'StudyClass';
    private static $primaryKey = 'id';
    private static $formName = 'formList_Class';
    private $showMethods = ['onReload', 'onSearch'];

    /**
     * Class constructor
     * Creates the page, the form and the listing
     */
    public function __construct()
    {
        parent::__construct();

        // creates a Datagrid
        $this->datagrid        = new TDataGrid;
        $this->datagrid        = new BootstrapDatagridWrapper($this->datagrid);
        $this->filter_criteria = new TCriteria;
        $this->datagrid->disableHtmlConversion();

        $this->datagrid->style = 'width: 100%';
        $this->datagrid->setHeight(320);

        $column_id             = new TDataGridColumn('id', "Código", 'center' , '70px');
        $column_name           = new TDataGridColumn('name', "Nome", 'left');
        //$column_institution_id = new TDataGridColumn('institution->name', "Instituição", 'left');
        //$column_teacher_id     = new TDataGridColumn('teacher_id', "Professor", 'left');

        //$column_teacher_id->setTransformer([$this, 'getTeacherName']);

        $order_id = new TAction(array($this, 'onReload'));
        $order_id->setParameter('order', 'id');
        $column_id->setAction($order_id);

        $this->datagrid->addColumn($column_id);
        $this->datagrid->addColumn($column_name);
        //$this->datagrid->addColumn($column_institution_id);
        // /$this->datagrid->addColumn($column_teacher_id);

        $action_onProcessData = new TDataGridAction(array($this, 'onProcessData'));
        $action_onProcessData->setUseButton(false);
        $action_onProcessData->setButtonClass('btn btn-default btn-sm');
        $action_onProcessData->setLabel("Processar dados");
        $action_onProcessData->setImage('fas:cogs #478fca');
        $action_onProcessData->setField(self::$primaryKey);

        $action_onExportCsv = new TDataGridAction(array($this, 'onExportCsv'));
        $action_onExportCsv->setUseButton(false);
        $action_onExportCsv->setButtonClass('btn btn-default btn-sm');
        $action_onExportCsv->setLabel("Exportar mensagens CSV");
        $action_onExportCsv->setImage('fa:table fa-fw blue #478fca');
        $action_onExportCsv->setField(self::$primaryKey);

        $action_group = new TDataGridActionGroup('Ações', 'fa:th');
        
        $action_group->addAction($action_onProcessData);
        $action_group->addAction($action_onExportCsv);

        // add the actions to the datagrid
        $this->datagrid->addActionGroup($action_group);

        // create the datagrid model
        $this->datagrid->createModel();

        // creates the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->enableCounters();
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());

        $panel = new TPanelGroup("Dados da turma");
        $panel->add($this->datagrid);

        $panel->addFooter($this->pageNavigation);

        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(TBreadCrumb::create(["Colaboração","Dados da turma"]));
        //$container->add($this->form);
        $container->add($panel);

        parent::add($container);
    }

    public function onProcessData($param)
    {
        try
        {
            // get the paramseter $key
            $key = $param['key'];
            // open a transaction with database
            TTransaction::open(self::$database);

            $csv = '';

            $csv .= " ; ;\n";

            $class = new StudyClass($key);
            
            $groups = $class->getGroups();

            if($groups)
            {
                foreach($groups as $group)
                {
                    $messages = Message::where('group_id', '=', $group->id)->load();

                    if(!$messages)
                    {
                        //new TMessage('error', 'Não há dados para serem processados');
                        continue;
                    }
                    
                    foreach ($messages as $message) 
                    {
                        $answers = Answer::where('message_id', '=', $message->id)->load();
                        
                        if($message->rating != 'irrelevante')
                        {
                            $message->message = str_replace('\n', '\ n', $message->message);
                            $message->message = preg_replace( "/\r|\n/", "", $message->message);
                            $message->message = trim(preg_replace('/\t+/', '', $message->message));
                            $message->message = str_replace(';', ':', $message->message);
                            
                            $csv .= str_replace(';;', ';', $message->message.';').
                            'P'.';'.
                            $message->id."\n";
                        }

                        if($answers)
                        {
                            foreach ($answers as $answer) 
                            {
                                if($answer->rating != 'irrelevante')
                                {
                                    $answer->answer = str_replace('\n', '\ n', $answer->answer);
                                    $answer->answer = preg_replace( "/\r|\n/", "", $answer->answer);
                                    $answer->answer = str_replace(';', ':', $answer->answer);
                                    $answer->answer = trim(preg_replace('/\t+/', '', $answer->answer));

                                    $csv .= str_replace(';;', ';', $answer->answer.';').
                                            'R'.';'.
                                            $answer->id."\n";
                                }
                            }
                        }
                    }
                }
            }

            $preferences_process_data = PreferencesProcessData::find(1);
            $training_base            = $preferences_process_data->training_base;

            if(file_exists('files/process/dados_turma'.$key.'.csv'))
            {
                unlink('files/process/dados_turma'.$key.'.csv');
            }

            file_put_contents('files/process/dados_turma'.$key.'.csv', $csv);
            if(!file_exists('files/data/dados_minerador'.$key.'.csv'))
            {
                copy('files/data/dados_minerador.csv', 'files/data/dados_minerador'.$key.'.csv');
            }
            
            $comando = passthru("python3 /var/www/html/collaby/minerador.py {$training_base} /var/www/html/collaby/files/process/dados_turma{$key}.csv /var/www/html/collaby/files/data/dados_minerador{$key}.csv");

            $file = fopen("/var/www/html/collaby/files/data/dados_minerador{$key}.csv", 'r');

            $first = TRUE;
            while(($line = fgetcsv($file, NULL, ",")) !== FALSE)
            {
                if ($first)
                {
                    $cabecalho = $line;
                    $first     = FALSE;
                }
                else
                {
                    $student_id = '';
                    if($line[1] == 'P')
                    {
                        $message    = new Message($line[2]);
                        $student_id = $message->student_id;
                        $group_id   = $message->group_id;
                    }
                    else
                    {
                        $answer = new Answer($line[2]);
                        $student_id = $answer->student_id;
                        $group_id   = $answer->message->group_id;
                    }

                    $process_data                 = new ProcessData();
                    $process_data->type           = $line[1];
                    $process_data->description    = $line[0];
                    $process_data->description_id = $line[2];
                    $process_data->date_process   = date('Y-m-d H:i:s');
                    $process_data->prob_0         = $line[3];
                    $process_data->prob_1         = $line[4];
                    $process_data->student_id     = $student_id;
                    $process_data->group_id       = $group_id;
                    $process_data->class_id       = $class->id;
                    $process_data->count_chars    = mb_strlen($line[0]);
                    $process_data->store();
                }
            }
            // close the transaction
            TTransaction::close();

            // shows the success message
            new TMessage('info', 'Dados processados! Para ver o resultado acesse o dashboard.');
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public function onExportCsv($param = null) 
    {
        try
        {
            TTransaction::open(self::$database);

            $key = $param['key'];

            $file = 'tmp/'.'turma'.$key.'-'.uniqid().'.csv';

            $class = new StudyClass($key);

            $csv  = '';
            $csv .= "Turma;Grupo;Cód aluno;Aluno;Tipo (pergunta ou resposta);Data/Hora;Mensagem;Nº de caracteres;\n";
            
            $groups = $class->getGroups();

            if($groups)
            {
                foreach($groups as $group)
                {
                    $messages = Message::where('group_id', '=', $group->id)->load();

                    if(!$messages)
                    {
                        //new TMessage('error', 'Não há dados para serem processados');
                        continue;
                    }

                    foreach ($messages as $message) 
                    {
                        $answers = Answer::where('message_id', '=', $message->id)->load();
                        
                        $message->message = str_replace('\n', '\ n', $message->message);
                        $message->message = preg_replace( "/\r|\n/", "", $message->message);
                        $message->message = trim(preg_replace('/\t+/', '', $message->message));
                        $message->message = str_replace(';', ':', $message->message);

                        $count_chars = !empty($message->count_chars) ? $message->count_chars : strlen($message->message);

                        $csv .= $class->name.';'.$group->name.';'.$message->student_id.';'.$message->student_name.';'.'P;'.$message->date_message.';'.str_replace(';;', ';', $message->message.';').$count_chars.";\n";

                        if($answers)
                        {
                            foreach ($answers as $answer) 
                            {
                                $answer->answer = str_replace('\n', '\ n', $answer->answer);
                                $answer->answer = preg_replace( "/\r|\n/", "", $answer->answer);
                                $answer->answer = str_replace(';', ':', $answer->answer);
                                $answer->answer = trim(preg_replace('/\t+/', '', $answer->answer));

                                $count_chars = !empty($answer->count_chars) ? $answer->count_chars : strlen($answer->answer);

                                $csv .= $class->name.';'.$group->name.';'.$answer->student_id.';'.$answer->student->name.';'.'R;'.$answer->date_answer.';'.str_replace(';;', ';', $answer->answer.';').$count_chars.";\n";
                            }
                        }
                    }
                }
            }

            TTransaction::close();
            file_put_contents($file, $csv);

            TPage::openFile($file);
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback();
        }
    }

    /**
     * Load the datagrid with data
     */
    public function onReload($param = NULL)
    {
        try
        {
            // open a transaction with database 'collaby'
            TTransaction::open(self::$database);

            // creates a repository for Class
            $repository = new TRepository(self::$activeRecord);
            $limit = 20;

            $criteria = clone $this->filter_criteria;

            if (empty($param['order']))
            {
                $param['order'] = 'id';    
            }

            if (empty($param['direction']))
            {
                $param['direction'] = 'desc';
            }

            $criteria->setProperties($param); // order, offset
            $criteria->setProperty('limit', $limit);

            if($filters = TSession::getValue(__CLASS__.'_filters'))
            {
                foreach ($filters as $filter) 
                {
                    $criteria->add($filter);       
                }
            }

            $user_id = ApplicationService::getUserID();
            $groups = ApplicationService::getUserGroupsIDS();
            if(in_array(3, $groups))
            {
                $criteria->add(new TFilter('teacher_id', '=', $user_id));
                //$criteria->add(new TFilter('active', '=', TRUE));
            }

            // load the objects according to criteria
            $objects = $repository->load($criteria, FALSE);

            $this->datagrid->clear();
            if ($objects)
            {
                // iterate the collection of active records
                foreach ($objects as $object)
                {
                    // add the object inside the datagrid

                    $this->datagrid->addItem($object);

                }
            }

            // reset the criteria for record count
            $criteria->resetProperties();
            $count= $repository->count($criteria);

            $this->pageNavigation->setCount($count); // count of records
            $this->pageNavigation->setProperties($param); // order, page
            $this->pageNavigation->setLimit($limit); // limit

            // close the transaction
            TTransaction::close();
            $this->loaded = true;
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public function onShow($param = null)
    {

    }

    /**
     * method show()
     * Shows the page
     */
    public function show()
    {
        // check if the datagrid is already loaded
        if (!$this->loaded AND (!isset($_GET['method']) OR !(in_array($_GET['method'],  $this->showMethods))) )
        {
            if (func_num_args() > 0)
            {
                $this->onReload( func_get_arg(0) );
            }
            else
            {
                $this->onReload();
            }
        }
        parent::show();
    }

}

