<?php

class Message extends TRecord
{
    const TABLENAME  = 'message';
    const PRIMARYKEY = 'id';
    const IDPOLICY   =  'serial'; // {max, serial}

    private $group;
    private $activity;
    private $student;

    const RELEVANT     = 2;
    const VERYRELEVANT = 3;

    const RATING = [0 => 'irrelevante',
                    1 => 'conversa_social',
                    2 => 'relevante',
                    3 => 'muito_relevante'];

    const RATING_ICON = [0 => 'trash',
                         1 => 'comments',
                         2 => 'thumbs-up',
                         3 => 'star'];

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('message');
        parent::addAttribute('group_id');
        parent::addAttribute('activity_id');
        parent::addAttribute('student_id');
        parent::addAttribute('date_message');
        parent::addAttribute('count_chars');
        parent::addAttribute('rating');
    }

    public static function getAnswersMessage($user_id)
    {
        $answer_message = [];
        $answers = Answer::where('message_id', 'IN', "(SELECT id FROM message WHERE student_id = {$user_id})")
                         ->where('student_id', '<>', $user_id);

        $answers = $answers->select(['DISTINCT id'])->orderBy('id', 'desc')->load();

        if($answers)
        {
            foreach ($answers as $answer) 
            {
                $answer_message[] = $answer;
            }
        }

        return $answer_message;
    }

    public static function getMessageBySdudentGroup($user_id, $message_id = NULL, $answer_id = NULL)
    {
        $answer_message = [];
        $messages = StudentGroup::where('student_id', '=', $user_id);
        $messages = $messages->select(['DISTINCT group_id'])->orderBy('group_id', 'asc')->load();
        if($messages)
        {
            foreach ($messages as $key => $message) 
            {
                $group_id = $message->group_id;

                $criteria = new TCriteria;
                $criteria->add(new TFilter('group_id', '=', $group_id));

                $messages = Message::getObjects($criteria);

                if(!$messages)
                {
                    break;
                }
                foreach ($messages as $message) 
                {
                    $answer_message[] = $message;
                }
            }
        }

        return $answer_message;
    }

    public static function getMessageInfo($message_id)
    {
        $message = new Message($message_id, FALSE);
        $student_name = self::getStudentName($message->id);

        $first_name = explode(' ', $student_name)[0];
        
        $message_info = '<b>'.$first_name.':</b> '.$message->message;

        return $message_info;
    }


    public static function getTotalAnswers($message_id)
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('message_id', '=', $message_id));

        $total_answers = count(Answer::getObjects($criteria));

        return $total_answers;
    }

    public static function getStudentName($message_id)
    {
        $message = new Message($message_id, FALSE);

        TTransaction::open('permission');

        $name = SystemUsers::where('id', '=', $message->student_id)->first()->name;

        TTransaction::close();
        
        return $name;
    }

    /**
     * Method set_group
     * Sample of usage: $var->group = $object;
     * @param $object Instance of StudentGroup
     */
    public function set_group(Group $object)
    {
        $this->group = $object;
        $this->group_id = $object->id;
    }

    /**
     * Method get_group
     * Sample of usage: $var->group->attribute;
     * @returns Group instance
     */
    public function get_group()
    {
    
        // loads the associated object
        if (empty($this->group))
            $this->group = new Group($this->group_id);
    
        // returns the associated object
        return $this->group;
    }
    /**
     * Method set_activity
     * Sample of usage: $var->activity = $object;
     * @param $object Instance of Activity
     */
    public function set_activity(Activity $object)
    {
        $this->activity = $object;
        $this->activity_id = $object->id;
    }

    /**
     * Method get_activity
     * Sample of usage: $var->activity->attribute;
     * @returns Activity instance
     */
    public function get_activity()
    {
    
        // loads the associated object
        if (empty($this->activity))
            $this->activity = new Activity($this->activity_id);
    
        // returns the associated object
        return $this->activity;
    }

    public function set_student(SystemUsers $object)
    {
        $this->student = $object;
        $this->student_id = $object->id;
    }

    public function get_student()
    {
    
        // loads the associated object
        if (empty($this->student))
            $this->student = new SystemUsers($this->student_id);
    
        // returns the associated object
        return $this->student;
    }   

    public function get_student_name()
    {
    
        // loads the associated object
        if (empty($this->student))
            $this->student = new SystemUsers($this->student_id);
    
        // returns the associated object
        return $this->student->name;
    }
}

