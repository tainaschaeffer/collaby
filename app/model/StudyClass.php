<?php

class StudyClass extends TRecord
{
    const TABLENAME  = 'class';
    const PRIMARYKEY = 'id';
    const IDPOLICY   =  'serial'; // {max, serial}

    private $institution;

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('name');
        parent::addAttribute('institution_id');
        parent::addAttribute('teacher_id');
        parent::addAttribute('active');
    }
    
    /**
     * Method set_institution
     * Sample of usage: $var->institution = $object;
     * @param $object Instance of Institution
     */
    public function set_institution(Institution $object)
    {
        $this->institution = $object;
        $this->institution_id = $object->id;
    }

    /**
     * Method get_institution
     * Sample of usage: $var->institution->attribute;
     * @returns Institution instance
     */
    public function get_institution()
    {
    
        // loads the associated object
        if (empty($this->institution))
            $this->institution = new Institution($this->institution_id);
    
        // returns the associated object
        return $this->institution;
    }

    /**
     * Method getActivitys
     */
    public function getActivitys()
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('class_id', '=', $this->id));
        return Activity::getObjects( $criteria );
    }
    /**
     * Method getGroups
     */
    public function getGroups()
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('class_id', '=', $this->id));
        return Group::getObjects( $criteria );
    }
    /**
     * Method getStudentClasss
     */
    public function getStudentClasss()
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('class_id', '=', $this->id));
        return StudentClass::getObjects( $criteria );
    }

    public static function getCountRelevantInterventions($class_id)
    {
        $messages = Message::where('group_id', 'IN', "(SELECT id FROM groups WHERE class_id = {$class_id})")
                           ->where('rating', 'IN', "NOESC: ('relevante', 'muito_relevante')");
        $messages = $messages->select(['DISTINCT id'])->orderBy('id', 'desc')->load();

        $answers = Answer::where('message_id', 'IN', "(SELECT message.id FROM message, groups WHERE message.group_id = groups.id AND groups.class_id = {$class_id})")
                           ->where('rating', 'IN', "NOESC: ('relevante', 'muito_relevante')");
        $answers = $answers->select(['DISTINCT id'])->orderBy('id', 'desc')->load();

        return count($messages) + count($answers);
    }

    public static function getCountInterventions($class_id)
    {
        $messages = Message::where('group_id', 'IN', "(SELECT id FROM groups WHERE class_id = {$class_id})");
        $messages = $messages->select(['DISTINCT id'])->orderBy('id', 'desc')->load();

        $answers = Answer::where('message_id', 'IN', "(SELECT message.id FROM message, groups WHERE message.group_id = groups.id AND groups.class_id = {$class_id})");
        $answers = $answers->select(['DISTINCT id'])->orderBy('id', 'desc')->load();

        return count($messages) + count($answers);
    }

    public function get_description()
    {
        $active = ($this->active) ? '(Ativa)' : '(Inativa)';
        return $this->name.' '.$active;
    }

    public static function hasProcessData($class_id)
    {
        $countProcessData = ProcessData::where('class_id', '=', $class_id)->count();
        return ($countProcessData > 0) ?: false;
    }
}

