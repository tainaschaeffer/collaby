<?php

class Institution extends TRecord
{
    const TABLENAME  = 'institution';
    const PRIMARYKEY = 'id';
    const IDPOLICY   =  'serial'; // {max, serial}

    

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('name');
            
    }

    /**
     * Method getClasss
     */
    public function getClasss()
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('institution_id', '=', $this->id));
        return StudyClass::getObjects( $criteria );
    }

    
}

