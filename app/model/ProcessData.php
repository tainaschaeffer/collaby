<?php

class ProcessData extends TRecord
{
    const TABLENAME  = 'process_data';
    const PRIMARYKEY = 'id';
    const IDPOLICY   =  'serial'; // {max, serial}

    private $student;

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('type');
        parent::addAttribute('description');
        parent::addAttribute('description_id');
        parent::addAttribute('date_process');
        parent::addAttribute('prob_0');
        parent::addAttribute('prob_1');
        parent::addAttribute('student_id');
        parent::addAttribute('group_id');   
        parent::addAttribute('class_id');   
        parent::addAttribute('count_chars');   
    }

    public function set_student(SystemUsers $object)
    {
        $this->student = $object;
        $this->student_id = $object->id;
    }

    public function get_student()
    {
    
        // loads the associated object
        if (empty($this->student))
            $this->student = new SystemUsers($this->student_id);
    
        // returns the associated object
        return $this->student;
    }   

    public function get_student_name()
    {
    
        // loads the associated object
        if (empty($this->student))
            $this->student = new SystemUsers($this->student_id);
    
        // returns the associated object
        return $this->student->name;
    }

    public static function getResultsByClassAndDateProcess($class_id, $date_process)
    {
        $preferences_process_data      = PreferencesProcessData::find(1);
        $solicitude_message_percentage = ($preferences_process_data->solicitude_message_percentage/100);
        $participation_percentage      = ($preferences_process_data->participation_percentage/100);
        $relevance_percentage          = ($preferences_process_data->relevance_percentage/100);

        $conn    = TTransaction::get();
        $prepare = $conn->prepare(file_get_contents('app/resources/sql/analise_indicadores_solicitude.sql'));
        $prepare->execute(['class_id'                      => $class_id,
                           'date_process'                  => $date_process,
                           'solicitude_message_percentage' => $solicitude_message_percentage,
                           'participation_percentage'      => $participation_percentage,
                           'relevance_percentage'          => $relevance_percentage]);

        return $prepare->fetchAll(PDO::FETCH_OBJ);
    }

    public static function getResultsByStudentClassAndDateProcess($class_id, $date_process, $system_users_id)
    {
        $preferences_process_data      = PreferencesProcessData::find(1);
        $solicitude_message_percentage = ($preferences_process_data->solicitude_message_percentage/100);
        $participation_percentage      = ($preferences_process_data->participation_percentage/100);
        $relevance_percentage          = ($preferences_process_data->relevance_percentage/100);

        $conn    = TTransaction::get();
        $prepare = $conn->prepare(file_get_contents('app/resources/sql/analise_indicadores_solicitude_aluno.sql'));
        $prepare->execute(['class_id'                      => $class_id,
                           'date_process'                  => $date_process,
                           'system_users_id'               => $system_users_id,
                           'solicitude_message_percentage' => $solicitude_message_percentage,
                           'participation_percentage'      => $participation_percentage,
                           'relevance_percentage'          => $relevance_percentage]);

        return $prepare->fetchAll(PDO::FETCH_OBJ)[0];
    }
}

