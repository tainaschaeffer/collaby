<?php

use Adianti\Database\TTransaction;

/**
 * GroupCompositionView
 *
 * @version    1.0
 * @package    samples
 * @subpackage tutor
 * @author     Artur Comunello
 * @author     Pablo Dall'Oglio
 * @copyright  Copyright (c) 2006 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */


class GroupCompositionView extends TPage
{
    private static $formName = 'form_GroupComposition';

    private $container;
    private $form;
    private $kanbanGroups;

    public function __construct($param)
    {
        parent::__construct();

        $this->container = new TVBox;
        $this->container->style = 'width: 100%';
        $this->container->add(TBreadCrumb::create(['Colaboração', 'Composição de grupos']));

        $this->form = new BootstrapFormBuilder(self::$formName, 'Filtros');

        $btn_onsearch = $this->form->addAction('Gerar grupos', new TAction([__CLASS__, 'onFilter'], $param), 'fa:users #ffffff');
        $btn_onsearch->addStyleClass('btn-primary');

        $this->makeWindowFilters();

        $this->container->add($this->form);

        parent::add($this->container);
    }

    private function makeWindowFilters()
    {
        $this->form->appendPage('Geral');

        $criteria_class = new TCriteria;
        $criteria_group = new TCriteria;

        $user_id = ApplicationService::getUserID();
        $groups  = ApplicationService::getUserGroupsIDS();
        if(in_array(3, $groups))
        {
            $criteria_class->add(new TFilter('teacher_id', '=', $user_id));
        }

        $class_id     = new TDBCombo('class_id', 'collaby', 'StudyClass', 'id', '{name}','id asc', $criteria_class);

        $class_id->setSize('100%');

        $row         = $this->form->addFields([new TLabel('Turma*'), $class_id]);
        $row->layout = ['col-sm-12'];
    }

    public function onFilter($param)
    {
        try
        {
            TTransaction::open('collaby');

            $data = $this->form->getData();

            if(!$data->class_id)
            {
                new TMessage('error', "O campo Turma é obrigatório");
                return;
            }

            if(StudyClass::hasProcessData($data->class_id))
            {
                new TMessage('error', "A turma selecionada já possui dados processados. Não será possível alterar a composição dos grupos automaticamente.");
                return;
            }

            $this->generateGroups($data->class_id);
            $this->makeKanbanGroups($data);

            $this->form->setData($data);

            TTransaction::close();
        }
        catch(Execption $e)
        {
            $this->form->setData($data);
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public function generateGroups($class_id)
    {
        try 
        {
            TTransaction::open('collaby');

            $groups = Group::where('class_id', '=', $class_id)->load();
            if(!$groups)
            {
                new TMessage('error', "A turma selecionada não possui grupos! Você deve gerá-los antes de utilizar esta funcionalidade.");
                return;
            }

            $students = StudentClass::getStudentByClassOrderedByRole($class_id);
            if(!$students)
            {
                new TMessage('error', "A turma selecionada não possui alunos! Você deve inserí-los na turma antes de utilizar esta funcionalidade.");
            }

            $studentsOrdered = [];

            foreach($students as $student)
            {
                $rand = rand();
                $studentsOrdered["{$student->role}{$rand}"] = $student;
            }
            
            ksort($studentsOrdered);

            $arrayGroups = [];
            $countGroups = 1;
            foreach($groups as $group)
            {
                // //remove all students from the group
                StudentGroup::where('group_id', '=', $group->id)->delete();

                $arrayGroups[$countGroups] = $group->id;
                $countGroups++;
            }
            
            $countStudentGroups = 0;
            foreach($studentsOrdered as $student)
            {
                $countStudentGroups++;
                if(!array_key_exists($countStudentGroups, $arrayGroups))
                {
                    $countStudentGroups = 1;
                }

                $group_id = $arrayGroups[$countStudentGroups];
                
                if(empty(StudentGroup::getGroupsByStudentClass($student->student_id, $student->class_id)))
                {
                    $objStudent             = new StudentGroup();
                    $objStudent->student_id = $student->student_id;
                    $objStudent->group_id   = $group_id;

                    $objStudent->store(); 
                }
            }

            new TMessage('info', "A composição dos grupos foi finalizada com sucesso.");
            
            TTransaction::close();
        }
        catch(Execption $e)
        {
            $this->form->setData($data);
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public function makeKanbanGroups($data)
    {
        try
        {
            if(isset($data->class_id))
            {
                TTransaction::open('collaby');

                $this->kanbanGroups = new TKanban;

                $groups = Group::where('class_id', '=', $data->class_id)->load();

                $stages = [];
                foreach($groups as $group)
                {
                    $stages[] = ['id' => $group->id, 'title' => $group->name];
                }

                foreach ($stages as $stage)
                {
                    $this->kanbanGroups->addStage($stage['id'], $stage['title']);
                    $students = StudentGroup::getStudentsByGroup($stage['id']);

                    if(!$students)
                    {
                        new TMessage('error', "Erro ao buscar os alunos!");
                        return;
                    }

                    $items = [];
                    foreach($students as $student)
                    {
                        $student_name = SystemUsers::find($student->student_id)->name;
                        $role = StudentClass::getRoleStudent($student->student_id, $data->class_id);

                        $student_name .= " ({$role})";

                        $color = '#A9A9A9';
                        $description = '';

                        $items[] = ['student_id' => $student->student_id, 'group_id' => $student->group_id, 'title' => $student_name, 'content' => $description, 'color' => $color];
                    }

                    foreach ($items as $item)
                    {
                        $this->kanbanGroups->addItem($item['student_id'], $item['group_id'], $item['title'], $item['content'], $item['color']);
                    }

                    $this->kanbanGroups->setItemDropAction(new TAction([$this, 'onUpdateItemDrop']));

                }

                TTransaction::close();

                parent::add($this->kanbanGroups);
            }
        }
        catch(Execption $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public static function onUpdateItemDrop($param)
    {
        try
        {
            TTransaction::open('collaby'); // open a transaction

            $group_id   = $param['stage_id'];
            $student_id = $param['id'];

            //StudentGroup::where('group_id', '=', $group_id)->delete();
            $group = new Group($group_id);

            if($student_id)
            {
                $groups = StudentGroup::getGroupsByStudentClass($student_id, $group->class_id);
                if($groups)
                {
                    foreach($groups as $group)
                    {
                        if($group->group_id <> $group_id)
                        {
                            StudentGroup::where('group_id', '=', $group->group_id)
                                ->where('student_id', '=', $student_id)->delete();
                        }
                    }
                }

                $object_student             = new StudentGroup();
                $object_student->student_id = $student_id;
                $object_student->group_id   = $group_id;

                $object_student->store();
            }

            TTransaction::close(); // close the transaction

            new TMessage('info', "Aluno movido de grupo!");

        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            $this->form->setData( $this->form->getData() ); // keep form data
            TTransaction::rollback(); // undo all pending operations
        }
    }

}
