<?php

class StudentGroup extends TRecord
{
    const TABLENAME  = 'student_group';
    const PRIMARYKEY = 'id';
    const IDPOLICY   =  'serial'; // {max, serial}

    private $group;

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('group_id');
        parent::addAttribute('student_id');
    }

    public static function getStudentsByGroup($group_id)
    {
        return StudentGroup::where('group_id', '=', $group_id)->load();
    }

    public static function getGroupsByStudent($student_id)
    {
        $sql = "(SELECT student_group.student_id 
                    FROM student_group, 
                        student_class, 
                        class
                WHERE student_group.student_id = student_class.student_id
                    AND student_class.class_id = class.id
                    AND class.active IS TRUE
                    AND student_class.student_id = {$student_id})";

        return StudentGroup::where('student_id', 'IN', $sql)->load();
    }

    public static function getGroupsByStudentClass($student_id, $class_id = NULL)
    {
        $sql = "(SELECT student_group.student_id 
                  FROM groups, 
                       student_group 
                 WHERE groups.id = student_group.group_id 
                   AND student_group.student_id = {$student_id}
                   AND groups.class_id = {$class_id})";

        return StudentGroup::where('student_id', 'IN', $sql)->load();
    }

    public static function getStudentInGroup($student_id, $group_id)
    {
        return StudentGroup::where('group_id', '=', $group_id)
                            ->where('student_id', '=', $student_id)
                            ->first();    
    }

    public static function addStudentInGroup($student_id, $group_id)
    {
        $student             = new StudentGroup();
        $student->student_id = $student_id;
        $student->group_id   = $group_id;

        $student->store();  
    }

    /**
     * Method set_group
     * Sample of usage: $var->group = $object;
     * @param $object Instance of group
     */
    public function set_group(Group $object)
    {
        $this->group = $object;
        $this->group_id = $object->id;
    }

    /**
     * Method get_group
     * Sample of usage: $var->group->attribute;
     * @returns group instance
     */
    public function get_group()
    {
    
        // loads the associated object
        if (empty($this->group))
            $this->group = new Group($this->group_id);
    
        // returns the associated object
        return $this->group;
    }   

    public function get_group_name()
    {
    
        // loads the associated object
        if (empty($this->group))
            $this->group = new Group($this->group_id);
    
        // returns the associated object
        return $this->group->name;
    }   
}