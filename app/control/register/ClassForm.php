<?php

class ClassForm extends TPage
{
    protected $form;
    private $formFields = [];
    private static $database = 'collaby';
    private static $activeRecord = 'StudyClass';
    private static $primaryKey = 'id';
    private static $formName = 'form_Class';

    private $students_list;
    //private $students_class_list;

    const CAMPOS_INDIVIDUAL = array('students_list');
    const CAMPOS_LOTE       = array('file');
    const PATH              = 'files/import/class/';
    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct( $param )
    {
        parent::__construct();

        // creates the form
        $this->form = new BootstrapFormBuilder(self::$formName);
        // define the form title
        $this->form->setFormTitle("Cadastro de turma");

        $criteriaTeacher = new TCriteria;
        $criteriaTeacher->add(new TFilter('id', 'IN', "(SELECT system_user_group.system_user_id FROM system_user_group WHERE system_user_group.system_group_id = 3)"));

        $criteriaStudent = new TCriteria;
        $criteriaStudent->add(new TFilter('id', 'IN', "(SELECT system_user_group.system_user_id FROM system_user_group WHERE system_user_group.system_group_id = 2)"));

        $id               = new TEntry('id');
        $name             = new TEntry('name');
        $teacher_id       = new TDBUniqueSearch('teacher_id', 'permission', 'SystemUsers', 'id', 'name', 'name', $criteriaTeacher);
        $institution_id   = new TDBCombo('institution_id', 'collaby', 'Institution', 'id', '{name}','id asc'  );
        $import           = new TRadioGroup('import');
        $file             = new TFile('file');
        $help_file        = new TElement('i');
        $help_file->style = 'color:#3691f7';
        $help_file->class = 'material-icons';
        $help_file->title = $this->getInfo();
        $help_file->add('info_outline');

        $uniq       = new THidden('uniq[]');
        $student_id = new TDBUniqueSearch('student_id[]', 'permission', 'SystemUsers', 'id', 'name', 'name', $criteriaStudent);
        $grade_ini  = new TEntry('grade_ini[]');
        $grade_end  = new TEntry('grade_end[]');
        $role  = new TCombo('role[]');

        $role->addItems(['A' => 'Alfa', 'B' => 'Beta', 'C' => 'Gama']);

        $name->addValidation("Nome", new TRequiredValidator()); 
        $institution_id->addValidation("Instituição", new TRequiredValidator()); 
        $teacher_id->addValidation("Professor", new TRequiredValidator()); 

        $name->setMaxLength(45);
        $id->setEditable(false);

        $id->setSize(100);
        $name->setSize('100%');
        $institution_id->setSize('100%');
        $teacher_id->setSize('100%');

        $student_id->setSize('100%');
        $grade_ini->setSize('100%');
        $grade_end->setSize('100%');
        $role->setSize('100%');

        $grade_ini->setNumericMask(2, ',', '.', TRUE);
        $grade_end->setNumericMask(2, ',', '.', TRUE);

        $import->setValue('no');
        $import->setLayout('horizontal');
        $import->setUseButton();
        $import->setChangeAction(new TAction([$this,'onChangeImport']));

        $import->addItems(['no' => 'Não', 'si' => 'Sim']);

        $file->setAllowedExtensions(['csv']);

        $row        = $this->form->addFields([new TLabel("Código:"), $id]);
        $row->style = ['col-sm-12'];

        $row        = $this->form->addFields([new TLabel("Nome:"), $name]);
        $row->style = ['col-sm-12'];

        //$row        = $this->form->addFields([new TLabel("Instituição:"), $institution_id]);
        //$row->style = ['col-sm-12'];

        $row        = $this->form->addFields([new TLabel("Professor:"), $teacher_id]);
        $row->style = ['col-sm-12'];

        $row         = $this->form->addFields([new TLabel('Importar alunos:'), $import]);
        $row->layout = ['col-sm-12'];

        $row         = $this->form->addFields([new TLabel('CSV:'), $file], [new TLabel('', NULL, NULL,NULL, '100%'), $help_file]);
        $row->layout = ['col-sm-11', 'col-sm-1'];
        $row->style  = 'display: none;';

        $this->student_list = new TFieldList;
        $this->student_list->generateAria();
        $this->student_list->width = '100%';
        $this->student_list->name  = 'student_list';
        $this->student_list->addField('<b>Id</b>', $uniq, ['width' => '0%', 'uniqid' => true]);
        $this->student_list->addField('<b>Aluno</b>', $student_id, ['width' => '50%']);
        $this->student_list->addField('<b>Nota inicial</b>', $grade_ini, ['width' => '20%']);
        $this->student_list->addField('<b>Nota final</b>', $grade_end, ['width' => '20%']);
        $this->student_list->addField('<b>Papel</b>', $role, ['width' => '10%']);
        
        $this->student_list->enableSorting();
        $this->student_list->addHeader();

        // add field list to the form
        $this->form->addContent([$this->student_list]);

        // create the form actions
        $btn_onsave  = $this->form->addAction("Salvar", new TAction([$this, 'onSave']), 'fas:save #ffffff');
        $btn_onsave->addStyleClass('btn-primary'); 
        $btn_onclear = $this->form->addAction("Limpar formulário", new TAction([$this, 'onClear']), 'fas:eraser #dd5a43');
        $btn_onsearch = $this->form->addAction('Voltar', new TAction(['ClassList', 'onShow']), 'fa:table #07488c');
        $btn_onsearch->addStyleClass('btn-default');

        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->class = 'form-container';
        $container->add(new TXMLBreadCrumb('menu.xml', 'ClassList'));
        $container->add($this->form);

        parent::add($container);
    }

    public static function onChangeImport($param)
    {
        if (isset($param['import']) AND $param['import'] == 'no')
        {
            self::camposIndividual(TRUE);
            self::camposLote(FALSE);
        }
        elseif(isset($param['import']) AND ($param['import'] == 'si'))
        {
            self::camposIndividual(FALSE);
            self::camposLote(TRUE);
        }
        else
        {
            self::camposIndividual(FALSE);
            self::camposLote(FALSE);
        }
    }

    private static function camposIndividual($fl_exibe = FALSE)
    {
        if(self::CAMPOS_INDIVIDUAL)
        {
            foreach(self::CAMPOS_INDIVIDUAL as $campo)
            {
                if($fl_exibe)
                {
                    TScript::create("$('.tfieldlist').show()");
                }
                else
                {
                    TScript::create("$('.tfieldlist').hide()");
                }
            }
        }
    }
    
    private static function camposLote($fl_exibe = FALSE)
    {
        if(self::CAMPOS_LOTE)
        {
            foreach(self::CAMPOS_LOTE as $campo)
            {
                if($fl_exibe)
                {
                    BootstrapFormBuilder::showField(self::$formName, $campo);
                }
                else
                {
                    BootstrapFormBuilder::hideField(self::$formName, $campo);
                }
            }
        }
    }

    public function getInfo()
    {
        $div = "<div style='text-align:left;line-height:2;'>
                    <b>Estrutura de importação:</b><br/>
                    &nbsp;&nbsp;&nbsp;file .CSV separado por ponto e vírgula '<b> ; </b>'<br/>
                    <b>Cabeçalho:</b><br/>
                    &nbsp;&nbsp;&nbsp;Código;Nome;Email;<br/>
                    <b>Valores:</b><br/>
                    &nbsp;&nbsp;&nbsp;571639;TAINÁ SCHAEFFER;taina.schaeffer@universo.univates.br;<br/>";
        return $div;
    }

    public function onSave($param = null) 
    {
        try
        {
            TTransaction::open(self::$database); // open a transaction

            $messageAction = null;

            $this->form->validate(); // validate form data

            $object = new StudyClass(); // create an empty object 

            $data = $this->form->getData(); // get form data as array

            $data->institution_id = 2;
            $object->fromArray( (array) $data); // load the object with data
            $object->store(); // save the object 

            // get the generated {PRIMARY_KEY}
            $data->id = $object->id; 

            if((isset($param['file']) AND $param['file']) AND (isset($param['import']) AND $param['import'] != 'no'))
            {
                $param['id'] = $data->id;
                self::saveFile($param);
            }

            elseif(isset($param["uniq"]) && $param["uniq"])
            {
                if($data->id)
                {
                    StudentClass::where('class_id', '=', $data->id)->delete();
                }
                
                foreach($param['uniq'] as $key => $aluno)
                {
                    if($param['student_id'][$key])
                    {
                        $grade_ini = str_replace(".", "", $param['grade_ini'][$key]);
                        $grade_ini = str_replace(",", ".", $grade_ini);

                        $grade_end = str_replace(".", "", $param['grade_end'][$key]);
                        $grade_end = str_replace(",", ".", $grade_end);

                        $object_student             = new StudentClass();
                        $object_student->student_id = $param['student_id'][$key];
                        $object_student->grade_ini  = $grade_ini;
                        $object_student->grade_end  = $grade_end;
                        $object_student->role       = $param['role'][$key];
                        $object_student->class_id   = $data->id;

                        $object_student->store();      
                    }
                }
            }

            $this->form->setData($data); // fill form data
            TTransaction::close(); // close the transaction

            $this->onEdit(['key' => $data->id]);

            new TMessage('info', "Registro salvo", $messageAction); 
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            $this->form->setData( $this->form->getData() ); // keep form data
            TTransaction::rollback(); // undo all pending operations
        }
    }

    private static function csvToArray($file)
    {
        $dados     = [];
        $cabecalho = [];
        $first     = TRUE;
        $atributos = 0;
        $file = fopen($file, 'r');
        while(($line = fgetcsv($file, NULL, ";")) !== FALSE)
        {
            if ($first)
            {
                // total de colunas menos a coluna de código de pessoa
                $atributos = count($line) - 1;
                $cabecalho = $line;
                $first     = FALSE;
            }
            else
            {
                $dados[ $line[0] ] = [];
                for ($i=1; $i <= $atributos; $i++)
                {
                    if(isset($line[$i]) AND trim($line[$i]))
                    {
                        $dados[$line[0]][trim($cabecalho[$i],'"')] = trim($line[$i],'"');
                    }
                }
            }

        }
        
        return $dados;
    }

    private static function saveFile($param)
    {
        $file = 'tmp/' . $param['file'];
        if(file_exists($file))
        {
            $data         = self::csvToArray($file);
            $array_students = array_keys($data);
            if(!$array_students)
            {
                throw new Exception('Não foi possível identificar as pessoas do file importado.');
            }

            foreach($data as $student)
            {
                $name         = $student['Nome'];
                $login        = explode("@", $student['Email'])[0];
                $email        = $student['Email'];

                $param['name']  = $name;
                $param['login'] = $login;
                $param['email'] = $email;

                if(!SystemUsers::newFromLogin($login))
                {
                    SystemUsers::saveUser($param);
                }

                $student_id = SystemUsers::newFromLogin($login)->id;

                if(!StudentClass::getStudentById($student_id))
                {
                    $object_student             = new StudentClass();
                    $object_student->student_id = $student_id;
                    $object_student->class_id   = $param['id'];
                    $object_student->store();            
                }
            }

            $new_path = self::PATH . date('Y_m_d_H_i_s') . '.csv';
            copy($file, $new_path);
        }
        else
        {
            throw new Exception("Arquivo não encontrado.");
        }
    }

    public function onEdit( $param )
    {
        try
        {
            if (isset($param['key']))
            {
                $key = $param['key'];  // get the parameter $key
                TTransaction::open(self::$database); // open a transaction

                $object = new StudyClass($key); // instantiates the Active Record 
                $object->import = 'no';

                $this->form->setData($object); // fill the form 

                //$this->makeStudents($key);

                $students = StudentClass::getStudentByClass($key);

                if($students)
                {
                    foreach ($students as $student) 
                    {
                        $objStudent             = new stdClass;
                        $objStudent->student_id = $student->student_id;
                        $objStudent->grade_ini  = $student->grade_ini;
                        $objStudent->grade_end  = $student->grade_end;
                        $objStudent->role       = $student->role;

                        $this->student_list->addDetail($objStudent);
                    }
                }
                else
                {
                    $this->student_list->addDetail(new stdClass);
                }
                
                $this->student_list->addCloneAction();

                TTransaction::close(); // close the transaction 
            }
            else
            {
                $this->form->clear();
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    /*private function makeStudents($class_id)
    {
        $selected_students = [];

        $students = StudentClass::getStudentByClass($class_id);

        if($students)
        {
            foreach ($students as $student) 
            {
                $system_user = new SystemUsers($student->student_id);
                $selected_students[$student->student_id] = $system_user->name;       
            }
        }

        $available_students = $this->getAvailableStudents($class_id);

        $this->students_list->addItems($available_students);
        $this->students_class_list->addItems($selected_students);
    }*/

    /*private function getAvailableStudents($class_id)
    {
        TTransaction::open('permission');

        $criteria = new TCriteria;
        $criteria->add(new TFilter('', '', "NOESC: id NOT IN (SELECT student_id FROM student_class WHERE class_id = $class_id AND student_id IN (SELECT system_user_id FROM system_user_group WHERE system_group_id = 2))"));

        $students = SystemUsers::getObjects($criteria);
        

        $available_students = [];
        if(!$students)
        {
            TTransaction::close();
            return $available_students;
        }

        foreach ($students as $student) 
        {
            if(in_array(2, $student->getSystemUserGroupIds()))
            {
                $available_students[$student->id] = $student->name;
            }
        }

        TTransaction::close();

        return $available_students;
    }*/

    /**
     * Clear form data
     * @param $param Request
     */
    public function onClear( $param )
    {
        $this->form->clear(true);

    }

    public function onShow($param = null)
    {

    } 

    public function onLoad($param = NULL)
    {
        try
        {
            TTransaction::open('permission');
            
            $login = TSession::getValue('login');
            $teacher_id = SystemUsers::idFromLogin($login);

            TTransaction::close();

            $data = new stdClass;
            $data->import = 'no';
            $data->teacher_id = $teacher_id;

            TForm::sendData(self::$formName, $data);

            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addDetail(new stdClass);
            $this->student_list->addCloneAction();

            //self::camposIndividual(FALSE);
            //self::camposLote(FALSE);       
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }
}

