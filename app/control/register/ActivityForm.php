<?php

class ActivityForm extends TPage
{
    protected $form;
    private $formFields = [];
    private static $database = 'collaby';
    private static $activeRecord = 'Activity';
    private static $primaryKey = 'id';
    private static $formName = 'form_Activity';

    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct( $param )
    {
        parent::__construct();

        // creates the form
        $this->form = new BootstrapFormBuilder(self::$formName);
        // define the form title
        $this->form->setFormTitle("Cadastro de atividades");

        $id          = new TEntry('id');
        $name        = new TEntry('name');
        $description = new TText('description');
        $class_id    = new TDBCombo('class_id', 'collaby', 'StudyClass', 'id', '{name}','id asc'  );

        $name->addValidation("Nome", new TRequiredValidator()); 
        $description->addValidation("Descrição", new TRequiredValidator()); 
        $class_id->addValidation("Turma", new TRequiredValidator()); 

        $name->setMaxLength(45);
        $id->setEditable(false);

        $id->setSize('10%');
        $name->setSize('100%');
        $class_id->setSize('100%');
        $description->setSize('100%');

        $row        = $this->form->addFields([new TLabel("Código:"), $id]);
        $row->style = ['col-sm-12'];

        $row        = $this->form->addFields([new TLabel("Nome:"), $name]);
        $row->style = ['col-sm-12'];

        $row        = $this->form->addFields([new TLabel("Descrição:"), $description]);
        $row->style = ['col-sm-12'];

        $row        = $this->form->addFields([new TLabel("Turma:"), $class_id]);
        $row->style = ['col-sm-12'];

        // create the form actions
        $btn_onsave = $this->form->addAction("Salvar", new TAction([$this, 'onSave']), 'fas:save #ffffff');
        $btn_onsave->addStyleClass('btn-primary'); 
        $btn_onclear = $this->form->addAction("Limpar formulário", new TAction([$this, 'onClear']), 'fas:eraser #dd5a43');
        $btn_onsearch = $this->form->addAction('Voltar', new TAction(['ActivityList', 'onShow']), 'fa:table #07488c');
        $btn_onsearch->addStyleClass('btn-default');

        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->class = 'form-container';
        $container->add(new TXMLBreadCrumb('menu.xml', 'ActivityList'));
        $container->add($this->form);

        parent::add($container);

    }

    public function onSave($param = null) 
    {
        try
        {
            TTransaction::open(self::$database); // open a transaction

            /**
            // Enable Debug logger for SQL operations inside the transaction
            TTransaction::setLogger(new TLoggerSTD); // standard output
            TTransaction::setLogger(new TLoggerTXT('log.txt')); // file
            **/

            $messageAction = null;

            $this->form->validate(); // validate form data

            $object = new Activity(); // create an empty object 

            $data = $this->form->getData(); // get form data as array
            $object->fromArray( (array) $data); // load the object with data

            $object->store(); // save the object 

            // get the generated {PRIMARY_KEY}
            $data->id = $object->id; 

            $this->form->setData($data); // fill form data
            TTransaction::close(); // close the transaction

            /**
            // To define an action to be executed on the message close event:
            $messageAction = new TAction(['className', 'methodName']);
            **/

            new TMessage('info', "Registro salvo", $messageAction); 

        }
        catch (Exception $e) // in case of exception
        {
            //</catchAutoCode> 

            new TMessage('error', $e->getMessage()); // shows the exception error message
            $this->form->setData( $this->form->getData() ); // keep form data
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function onEdit( $param )
    {
        try
        {
            if (isset($param['key']))
            {
                $key = $param['key'];  // get the parameter $key
                TTransaction::open(self::$database); // open a transaction

                $object = new Activity($key); // instantiates the Active Record 

                $this->form->setData($object); // fill the form 

                TTransaction::close(); // close the transaction 
            }
            else
            {
                $this->form->clear();
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    /**
     * Clear form data
     * @param $param Request
     */
    public function onClear( $param )
    {
        $this->form->clear(true);

    }

    public function onShow($param = null)
    {

    } 

}

