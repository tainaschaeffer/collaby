<?php

use Adianti\Database\TTransaction;

class DiscussionList extends TPage
{
    protected $form;
    protected $fab;
    private $formFields          = [];
    private static $db_database  = 'collaby';
    private static $ac_activeRecord = 'Message';
    private static $primaryKey   = 'id';
    private static $formName     = 'list_Message';
    private $showMethods         = ['onReload', 'onSearch'];

    // trait with onReload, onSearch, onDelete...
    use Adianti\Base\AdiantiStandardListTrait;

    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct()
    {
        parent::__construct();

        // creates the form
        $this->form = new BootstrapFormBuilder(self::$formName);
        
        TTransaction::open(self::$db_database);

        $user_id = ApplicationService::getUserID();
        $groups = ApplicationService::getUserGroupsIDS();
        if(in_array(3, $groups))
        {
            $criteriaGroup = new TCriteria;
            $criteriaGroup->add(new TFilter('id', 'IN', "(SELECT groups.id FROM groups, class WHERE groups.class_id = class.id and class.teacher_id = {$user_id})"));

            $criteriaActivity = new TCriteria;
            $criteriaActivity->add(new TFilter('id', 'IN', "(SELECT groups.id FROM groups, class WHERE groups.class_id = class.id and class.teacher_id = {$user_id})"));

            $group_id    = new TDBCombo('group_id', 'collaby', 'Group', 'id', '{name} - {class->name}','id asc', $criteriaGroup);
            $activity_id = new TDBCombo('activity_id', 'collaby', 'Activity', 'id', '{name} - {class->name}','id desc', $criteriaActivity);

            $group_id->setSize('100%');
            $activity_id->setSize('100%');

            $row        = $this->form->addFields([new TLabel("Grupo:"), $group_id]);
            $row->style = ['col-sm-12'];

            $row        = $this->form->addFields([new TLabel("Atividade:"), $activity_id]);
            $row->style = ['col-sm-12'];
        }

        $message = new TEntry('message');

        $message->setSize('100%');

        $row        = $this->form->addFields([new TLabel("Mensagem:"), $message]);
        $row->style = ['col-sm-12'];

        //create the form actions
        $btn_onsearch = $this->form->addAction("Buscar", new TAction([$this, 'onSearch']), 'fas:search #ffffff');
        $btn_onsearch->addStyleClass('btn-primary'); 

        //$btn_onnew = $this->form->addAction("Perguntar", new TAction(['DiscussionForm', 'onEdit']), 'fas:plus #69aa46');
        
        $btn_groups = $this->form->addAction("Meus grupos", new TAction([$this, 'onViewMyGroups']), 'fas:users #69aa46');

        $btn_activities = $this->form->addAction("Minhas atividades", new TAction([$this, 'onViewMyActivities']), 'fas:clipboard-list #69aa46');

        $this->form->addExpandButton('', 'fas: fa-arrows-alt-v');

        // creates a Datagrid
        $this->datagrid = new BootstrapDatagridWrapper(new TDataGrid);
        $this->filter_criteria = new TCriteria;

        $this->datagrid->style = 'width: 100%';
        //$this->datagrid->setHeight(320);

        $column_message = new TDataGridColumn('id', "Mensagem", 'left', '90%');
        $column_answers = new TDataGridColumn('id', "Respostas", 'center', '10%');

        $column_message->setTransformer(['Message', 'getMessageInfo']);
        $column_answers->setTransformer(['Message', 'getTotalAnswers']);

        $this->datagrid->addColumn($column_message);
        $this->datagrid->addColumn($column_answers);

        $action_onAnswerNow = new TDataGridAction(array('DiscussionList', 'onAnswer'));
        $action_onAnswerNow->setUseButton(false);
        $action_onAnswerNow->setButtonClass('btn btn-default btn-sm');
        $action_onAnswerNow->setLabel("Responder");
        $action_onAnswerNow->setImage('fa:plus #00BCD4');
        $action_onAnswerNow->setField(self::$primaryKey);

        $action_onParticipate = new TDataGridAction(array('DiscussionForm', 'onParticipate'));
        $action_onParticipate->setUseButton(false);
        $action_onParticipate->setButtonClass('btn btn-default btn-sm');
        $action_onParticipate->setLabel("Participar");
        $action_onParticipate->setImage('fa:arrow-right #00BCD4');
        $action_onParticipate->setField(self::$primaryKey);
        
        $action_onAnswer = new TDataGridAction(array($this, 'onAnswer'));
        $action_onAnswer->setUseButton(false);
        $action_onAnswer->setButtonClass('btn btn-default btn-sm');
        $action_onAnswer->setLabel("Participar");
        $action_onAnswer->setImage('fa:arrow-right #00BCD4');
        $action_onAnswer->setField(self::$primaryKey);

        $action_onEdit = new TDataGridAction(array('DiscussionForm', 'onEdit'));
        $action_onEdit->setUseButton(false);
        $action_onEdit->setButtonClass('btn btn-default btn-sm');
        $action_onEdit->setLabel("Editar");
        $action_onEdit->setImage('far:edit #478fca');
        $action_onEdit->setField(self::$primaryKey);
        $action_onEdit->setDisplayCondition(array($this, 'displayOption'));

        //$action_onView = new TDataGridAction([$this, 'onView'],   ['message_id' => '{id}'] );

        // add the actions to the datagrid
        $this->datagrid->addAction($action_onAnswerNow);
        $this->datagrid->addAction($action_onParticipate);
        //$this->datagrid->addAction($action_onView, 'Visualizar', 'fa:search blue');
        $this->datagrid->addAction($action_onEdit);

        // create the datagrid model
        $this->datagrid->createModel();

        // creates the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->enableCounters();
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        //$this->pageNavigation->setWidth($this->datagrid->getWidth());

        //$panel = new TPanelGroup;
        //$panel->add($this->datagrid);
        //$panel->addFooter($this->pageNavigation);
        
        if(StudentGroup::getGroupsByStudent($user_id))
        {
            $action = new TAction(['DiscussionForm', 'onEdit'], []);
            $this->fab = new TFabButton('my_fab','#29B6F6','fas:plus #fff', 'Ações', $action);
        }

        TTransaction::close();
        //$this->fab->addItem('Perguntar', $action, 'fas:plus', '#FFF');

        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add($this->form);
        $container->add($panel = TPanelGroup::pack('', $this->datagrid, $this->pageNavigation));
        $panel->getBody()->style = 'overflow-x:auto; overflow-y:auto;';
        $container->add($this->fab);
        parent::add($container);
    }

    public static function onViewMyActivities($param)
    {
        try
        {
            TTransaction::open(self::$db_database);

            $student_id = SystemUsers::newFromLogin(TSession::getValue('login'))->id;
            
            $student_classes = StudentClass::where('student_id', '=', "$student_id")
                                          ->where('class_id', 'IN', "NOESC: (SELECT id FROM class WHERE active IS TRUE)")
                                          ->groupBy('class_id, student_id, id')
                                          ->orderBy('class_id', 'asc')->load();

            $info = '';

            if($student_classes)
            {
                foreach($student_classes as $student_class)
                {
                    $data_activity = Activity::getLastActivity($student_class->class_id);

                    $info .= '<b>Turma:</b><br>'.$student_class->class->name.'<br>';

                    if($data_activity)
                    {
                        $info .= '<b>Atividade:</b><br>'.$data_activity[0]->name.'<br>';
                        $info .= '<b>Descrição:</b><br>'.$data_activity[0]->description.'<br>';
                        $info .= '<br>';
                    }
                    else
                    {
                        $info .= 'Não há atividades para listar<br>';
                    }
                }
            }

            TTransaction::close();
            
            $window = TWindow::create('Minhas atividades', 0.7, 0.7);
            $window->add($info);
            $window->show();
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public static function onViewMyGroups($param)
    {
        try
        {
            TTransaction::open(self::$db_database);

            $student_id = SystemUsers::newFromLogin(TSession::getValue('login'))->id;

            $student_groups = StudentGroup::where('student_id', '=', "$student_id")
                                          ->groupBy('group_id, student_id, id')
                                          ->orderBy('group_id', 'asc')->load();

            $info = '';

            if($student_groups)
            {
                foreach($student_groups as $student_group)
                {
                    $info .= '<b>Grupo:</b><br>'.$student_group->group->name.'<br>';

                    $students_group = StudentGroup::getStudentsByGroup($student_group->group_id);

                    if($students_group)
                    {
                        $info .= '<b>Integrantes:</b><br>';
                        foreach($students_group as $students)
                        {
                            $student = new SystemUsers($students->student_id);

                            $info .= $student->name.'<br>';
                        }
                    }

                    $info .= '<br>';
                }
            }

            TTransaction::close();
            
            $window = TWindow::create('Meus grupos', 0.7, 0.7);
            $window->add($info);
            $window->show();
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public function onAnswer($param)
    {
        TTransaction::open(self::$db_database);

        $message_id = $param['key'];
        $student_id = SystemUsers::newFromLogin(TSession::getValue('login'))->id;

        TTransaction::close();

        $dados = ['message_id'       => $message_id,
                  'student_id'       => $student_id,
                  'load_discussions' => FALSE];

        AdiantiCoreApplication::loadPage('AnswerForm', 'onLoad', $dados);
    }

    public function displayOption($object)
    {
        $student_id = SystemUsers::newFromLogin(TSession::getValue('login'))->id;

        if ($object->student_id == $student_id)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function onSearch($param)
    {
        // get the search form data
        $data = $this->form->getData();
        $filters = [];

        TSession::setValue(__CLASS__.'_filter_data', NULL);
        TSession::setValue(__CLASS__.'_filters', NULL);

        if (isset($data->message) AND ( (is_scalar($data->message) AND $data->message !== '') OR (is_array($data->message) AND (!empty($data->message)) )) )
        {

            $filters[] = new TFilter('message', 'ilike', "%{$data->message}%");// create the filter 
        }

        if (isset($data->group_id) AND ( (is_scalar($data->group_id) AND $data->group_id !== '') OR (is_array($data->group_id) AND (!empty($data->group_id)) )) )
        {

            $filters[] = new TFilter('group_id', '=', $data->group_id);// create the filter 
        }

        if (isset($data->activity_id) AND ( (is_scalar($data->activity_id) AND $data->activity_id !== '') OR (is_array($data->activity_id) AND (!empty($data->activity_id)) )) )
        {

            $filters[] = new TFilter('activity_id', '=', $data->activity_id);// create the filter 
        }

        $param = array();
        $param['offset']     = 0;
        $param['first_page'] = 1;

        // fill the form with data again
        $this->form->setData($data);

        // keep the search data in the session
        TSession::setValue(__CLASS__.'_filter_data', $data);
        TSession::setValue(__CLASS__.'_filters', $filters);

        $this->onReload($param);
    }

    /**
     * Load the datagrid with data
     */
    public function onReload($param = NULL)
    {
        try
        {
            // open a transaction with database 'collaby'
            TTransaction::open(self::$db_database);

            // creates a repository for Activity
            $repository = new TRepository(self::$ac_activeRecord);
            $limit      = 20;

            $criteria = clone $this->filter_criteria;

            if (empty($param['order']))
            {
                $param['order'] = 'id';    
            }

            if (empty($param['direction']))
            {
                $param['direction'] = 'desc';
            }

            $user_id = ApplicationService::getUserID();
            $groups = ApplicationService::getUserGroupsIDS();
            if(in_array(2, $groups))
            {
                $criteria->add(new TFilter('group_id', 'IN', "(SELECT group_id FROM student_group WHERE student_id = {$user_id})"));
                $criteria->add(new TFilter('activity_id', 'IN', '(SELECT activity.id FROM activity WHERE active IS NOT FALSE)'));
            }
            
            if(in_array(3, $groups))
            {
                $criteria->add(new TFilter('group_id', 'IN', "(SELECT groups.id FROM groups, class WHERE groups.class_id = class.id and class.teacher_id = {$user_id})"));
            }

            //$criteria->add(new TFilter('group_id', 'IN', "(SELECT group_id FROM student_group WHERE student_id = {$user_id} and student_id IN (SELECT system_user_id FROM system_user_group WHERE system_group_id IN (1, 3)))"));

            $criteria->setProperties($param); // order, offset
            $criteria->setProperty('limit', $limit);

            if($filters = TSession::getValue(__CLASS__.'_filters'))
            {
                foreach ($filters as $filter) 
                {
                    $criteria->add($filter);       
                }
            }


            // load the objects according to criteria
            $objects = $repository->load($criteria, FALSE);
            $this->datagrid->clear();
            if ($objects)
            {
                // iterate the collection of active records
                foreach ($objects as $object)
                {
                    // add the object inside the datagrid
                    $this->datagrid->addItem($object);
                }
            }

            // reset the criteria for record count
            $criteria->resetProperties();
            $count= $repository->count($criteria);

            $this->pageNavigation->setCount($count); // count of records
            $this->pageNavigation->setProperties($param); // order, page
            $this->pageNavigation->setLimit($limit); // limit

            // close the transaction
            TTransaction::close();
            $this->loaded = true;
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public function onShow($param = null)
    {

    }

    /**
     * method show()
     * Shows the page
     */
    public function show()
    {
        // check if the datagrid is already loaded
        if (!$this->loaded AND (!isset($_GET['method']) OR !(in_array($_GET['method'],  $this->showMethods))) )
        {
            if (func_num_args() > 0)
            {
                $this->onReload( func_get_arg(0) );
            }
            else
            {
                $this->onReload();
            }
        }
        parent::show();
    }
}