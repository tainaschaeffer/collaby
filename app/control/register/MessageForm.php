<?php

class MessageForm extends TPage
{
    protected $form;
    private $formFields = [];
    private static $database = 'collaby';
    private static $activeRecord = 'Message';
    private static $primaryKey = 'id';
    private static $formName = 'form_Message';

    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct( $param )
    {
        parent::__construct();

        // creates the form
        $this->form = new BootstrapFormBuilder(self::$formName);
        // define the form title
        $this->form->setFormTitle("Cadastro de message");


        $id = new TEntry('id');
        $subject = new TEntry('subject');
        $message = new TEntry('message');
        $student_class_id = new TDBCombo('student_class_id', 'collaby', 'StudentClass', 'id', '{id}','id asc'  );
        $activity_id = new TDBCombo('activity_id', 'collaby', 'Activity', 'id', '{id}','id asc'  );

        $subject->addValidation("Subject", new TRequiredValidator()); 
        $message->addValidation("Message", new TRequiredValidator()); 
        $student_class_id->addValidation("Student class id", new TRequiredValidator()); 
        $activity_id->addValidation("Activity id", new TRequiredValidator()); 

        $subject->setMaxLength(100);
        $id->setEditable(false);

        $id->setSize(100);
        $subject->setSize('100%');
        $message->setSize('100%');
        $activity_id->setSize('100%');
        $student_class_id->setSize('100%');

        $row1 = $this->form->addFields([new TLabel("Id:", null, '14px', null)],[$id]);
        $row2 = $this->form->addFields([new TLabel("Subject:", '#ff0000', '14px', null)],[$subject]);
        $row3 = $this->form->addFields([new TLabel("Message:", '#ff0000', '14px', null)],[$message]);
        $row4 = $this->form->addFields([new TLabel("Student class id:", '#ff0000', '14px', null)],[$student_class_id]);
        $row5 = $this->form->addFields([new TLabel("Activity id:", '#ff0000', '14px', null)],[$activity_id]);

        // create the form actions
        $btn_onsave = $this->form->addAction("Salvar", new TAction([$this, 'onSave']), 'fas:save #ffffff');
        $btn_onsave->addStyleClass('btn-primary'); 

        $btn_onclear = $this->form->addAction("Limpar formulário", new TAction([$this, 'onClear']), 'fas:eraser #dd5a43');

        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->class = 'form-container';
        // $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);

        parent::add($container);

    }

    public function onSave($param = null) 
    {
        try
        {
            TTransaction::open(self::$database); // open a transaction

            /**
            // Enable Debug logger for SQL operations inside the transaction
            TTransaction::setLogger(new TLoggerSTD); // standard output
            TTransaction::setLogger(new TLoggerTXT('log.txt')); // file
            **/

            $messageAction = null;

            $this->form->validate(); // validate form data

            $object = new Message(); // create an empty object 

            $data = $this->form->getData(); // get form data as array

            $data->count_chars = strlen($data->message);
            
            $object->fromArray( (array) $data); // load the object with data

            $object->store(); // save the object 

            // get the generated {PRIMARY_KEY}
            $data->id = $object->id; 

            $this->form->setData($data); // fill form data
            TTransaction::close(); // close the transaction

            /**
            // To define an action to be executed on the message close event:
            $messageAction = new TAction(['className', 'methodName']);
            **/

            new TMessage('info', "Registro salvo", $messageAction); 

        }
        catch (Exception $e) // in case of exception
        {
            //</catchAutoCode> 

            new TMessage('error', $e->getMessage()); // shows the exception error message
            $this->form->setData( $this->form->getData() ); // keep form data
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function onEdit( $param )
    {
        try
        {
            if (isset($param['key']))
            {
                $key = $param['key'];  // get the parameter $key
                TTransaction::open(self::$database); // open a transaction

                $object = new Message($key); // instantiates the Active Record 

                $this->form->setData($object); // fill the form 

                TTransaction::close(); // close the transaction 
            }
            else
            {
                $this->form->clear();
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    /**
     * Clear form data
     * @param $param Request
     */
    public function onClear( $param )
    {
        $this->form->clear(true);

    }

    public function onShow($param = null)
    {

    } 

}

