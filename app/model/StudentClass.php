<?php

class StudentClass extends TRecord
{
    const TABLENAME  = 'student_class';
    const PRIMARYKEY = 'id';
    const IDPOLICY   =  'serial'; // {max, serial}

    private $class;

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('class_id');
        parent::addAttribute('student_id');            
        parent::addAttribute('grade_ini');            
        parent::addAttribute('grade_end');         
        parent::addAttribute('role');   
    }

    static public function getStudentById($student_id)
    {
        return StudentClass::where('student_id', '=', $student_id)->first();
    }

    static public function getStudentByClass($class_id)
    {
        return StudentClass::where('class_id', '=', $class_id)->load();
    }

    static public function getAvailableStudentByClass($class_id)
    {
        return StudentClass::where('student_id', 'IN', "NOESC: (SELECT student_id FROM student_class WHERE class_id = $class_id AND student_id NOT IN (SELECT student_id FROM student_group, groups WHERE student_group.group_id = groups.id AND groups.class_id = $class_id))")->load();
    }

    static public function getStudentByClassOrderedByRole($class_id)
    {
        return StudentClass::where('student_id', 'IN', 
                    "NOESC: (SELECT student_id 
                               FROM student_class 
                              WHERE class_id = $class_id)")->orderBy('role', 'ASC')->load();
    }

    /**
     * Method set_class
     * Sample of usage: $var->class = $object;
     * @param $object Instance of Class
     */
    public function set_class(StudyClass $object)
    {
        $this->class = $object;
        $this->class_id = $object->id;
    }

    /**
     * Method get_class
     * Sample of usage: $var->class->attribute;
     * @returns Class instance
     */
    public function get_class()
    {
    
        // loads the associated object
        if (empty($this->class))
            $this->class = new StudyClass($this->class_id);
    
        // returns the associated object
        return $this->class;
    }

    /**
     * Method getMessages
     */
    public function getMessages()
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('student_class_id', '=', $this->id));
        return Message::getObjects( $criteria );
    }

    public static function getRoleStudent($student_id, $class_id)
    {
        $data = StudentClass::where('class_id', '=', $class_id)->where('student_id', '=', $student_id)->load();
        
        if($data[0]->role == 'A')
        {
            return 'Alfa';
        }
        if($data[0]->role == 'B')
        {
            return 'Beta';
        }
        if($data[0]->role == 'C')
        {
            return 'Gama';
        }
    }

    public static function hasRole($student_id, $role)
    {
        return StudentClass::where('student_id', '=', $student_id)->where('role', '=', $role)->load();
    }
}