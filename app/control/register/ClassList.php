<?php

class ClassList extends TPage
{
    private $form; // form
    private $datagrid; // listing
    private $pageNavigation;
    private $loaded;
    private $filter_criteria;
    private static $database = 'collaby';
    private static $activeRecord = 'StudyClass';
    private static $primaryKey = 'id';
    private static $formName = 'formList_Class';
    private $showMethods = ['onReload', 'onSearch'];

    /**
     * Class constructor
     * Creates the page, the form and the listing
     */
    public function __construct()
    {
        parent::__construct();
        // creates the form
        $this->form = new BootstrapFormBuilder(self::$formName);

        // define the form title
        $this->form->setFormTitle("Listagem de turma");

        $name = new TEntry('name');

        $name->setMaxLength(45);
        $name->setSize('100%');

        $row        = $this->form->addFields([new TLabel("Nome:"), $name]);
        $row->style = ['col-sm-12'];

        // keep the form filled during navigation with session data
        $this->form->setData( TSession::getValue(__CLASS__.'_filter_data') );

        $btn_onsearch    = $this->form->addAction("Buscar", new TAction([$this, 'onSearch']), 'fas:search #ffffff');
        $btn_onsearch->addStyleClass('btn-primary'); 
        $btn_onexportcsv = $this->form->addAction("Exportar como CSV", new TAction([$this, 'onExportCsv']), 'far:file-alt #000000');
        $btn_onshow      = $this->form->addAction("Cadastrar", new TAction(['ClassForm', 'onLoad']), 'fas:plus #69aa46');

        // creates a Datagrid
        $this->datagrid        = new TDataGrid;
        $this->datagrid        = new BootstrapDatagridWrapper($this->datagrid);
        $this->filter_criteria = new TCriteria;
        $this->datagrid->disableHtmlConversion();

        $this->datagrid->style = 'width: 100%';
        $this->datagrid->setHeight(320);

        $column_id             = new TDataGridColumn('id', "Código", 'center' , '70px');
        $column_name           = new TDataGridColumn('description', "Nome", 'left');
        //$column_institution_id = new TDataGridColumn('institution->name', "Instituição", 'left');
        $column_teacher_id     = new TDataGridColumn('teacher_id', "Professor", 'left');

        $column_teacher_id->setTransformer([$this, 'getTeacherName']);

        $order_id = new TAction(array($this, 'onReload'));
        $order_id->setParameter('order', 'id');
        $column_id->setAction($order_id);

        $this->datagrid->addColumn($column_id);
        $this->datagrid->addColumn($column_name);
        //$this->datagrid->addColumn($column_institution_id);
        $this->datagrid->addColumn($column_teacher_id);

        $action_onEdit = new TDataGridAction(array('ClassForm', 'onEdit'));
        $action_onEdit->setUseButton(false);
        $action_onEdit->setButtonClass('btn btn-default btn-sm');
        $action_onEdit->setLabel("Editar");
        $action_onEdit->setImage('far:edit #478fca');
        $action_onEdit->setField(self::$primaryKey);

        $action_onActive = new TDataGridAction(array('ClassList', 'onActive'));
        $action_onActive->setUseButton(false);
        $action_onActive->setButtonClass('btn btn-default btn-sm');
        $action_onActive->setLabel("Encerrar/reabrir turma");
        $action_onActive->setImage('fas:ban #dd5a43');
        $action_onActive->setField(self::$primaryKey);

        $action_onImport = new TDataGridAction(array($this, 'onImportCsv'));
        $action_onImport->setUseButton(false);
        $action_onImport->setButtonClass('btn btn-default btn-sm');
        $action_onImport->setLabel("Importar alunos");
        $action_onImport->setImage('far:file-alt #000000');
        $action_onImport->setField(self::$primaryKey);

        $action_onProcessData = new TDataGridAction(array('ClassList', 'onProcessData'));
        $action_onProcessData->setUseButton(false);
        $action_onProcessData->setButtonClass('btn btn-default btn-sm');
        $action_onProcessData->setLabel("Processar dados");
        $action_onProcessData->setImage('fas:cogs #478fca');
        $action_onProcessData->setField(self::$primaryKey);

        $action_onDelete = new TDataGridAction(array('ClassList', 'onDelete'));
        $action_onDelete->setUseButton(false);
        $action_onDelete->setButtonClass('btn btn-default btn-sm');
        $action_onDelete->setLabel("Excluir");
        $action_onDelete->setImage('fas:trash-alt #dd5a43');
        $action_onDelete->setField(self::$primaryKey);

        $action_group = new TDataGridActionGroup('Ações', 'fa:th');
        
        $action_group->addAction($action_onEdit);
        $action_group->addAction($action_onActive);
        //$action_group->addAction($action_onImport);
        //$action_group->addAction($action_onProcessData);
        $action_group->addAction($action_onDelete);
        
        // add the actions to the datagrid
        $this->datagrid->addActionGroup($action_group);

        // create the datagrid model
        $this->datagrid->createModel();

        // creates the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->enableCounters();
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());

        $panel = new TPanelGroup;
        $panel->add($this->datagrid);

        $panel->addFooter($this->pageNavigation);

        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(TBreadCrumb::create(["Básico","Turma"]));
        $container->add($this->form);
        $container->add($panel);

        parent::add($container);
    }

    public function onProcessData($param)
    {
        try
        {
            // get the paramseter $key
            $key = $param['key'];
            // open a transaction with database
            TTransaction::open(self::$database);

            $csv = '';

            $class = new StudyClass($key);
            
            $groups = $class->getGroups();

            if($groups)
            {
                foreach($groups as $group)
                {
                    $messages = Message::where('group_id', '=', $group->id)->load();

                    if(!$messages)
                    {
                        //new TMessage('error', 'Não há dados para serem processados');
                        continue;
                    }

                    foreach ($messages as $message) 
                    {
                        $answers = Answer::where('message_id', '=', $message->id)->load();
                        
                        $message->message = str_replace('\n', '\ n', $message->message);
                        $message->message = preg_replace( "/\r|\n/", "", $message->message);
                        $message->message = trim(preg_replace('/\t+/', '', $message->message));
                        $message->message = str_replace(';', ':', $message->message);

                        $csv .= str_replace(';;', ';', $message->message.';').
                                'P'.';'.
                                $message->id."\n";

                        if($answers)
                        {
                            foreach ($answers as $answer) 
                            {
                                $answer->answer = str_replace('\n', '\ n', $answer->answer);
                                $answer->answer = preg_replace( "/\r|\n/", "", $answer->answer);
                                $answer->answer = str_replace(';', ':', $answer->answer);
                                $answer->answer = trim(preg_replace('/\t+/', '', $answer->answer));

                                $csv .= str_replace(';;', ';', $answer->answer.';').
                                        'R'.';'.
                                        $answer->id."\n";
                            }
                        }
                    }
                }
            }

            $preferences_process_data = PreferencesProcessData::find(1);
            $training_base            = $preferences_process_data->training_base;

            file_put_contents('files/process/dados_turma'.$key.'.csv', $csv);
            if(!file_exists('files/data/dados_minerador'.$key.'.csv'))
            {
                copy('files/data/dados_minerador.csv', 'files/data/dados_minerador'.$key.'.csv');
            }
            
            $comando = passthru("python3 /var/www/html/collaby/minerador.py {$training_base} /var/www/html/collaby/files/process/dados_turma{$key}.csv /var/www/html/collaby/files/data/dados_minerador{$key}.csv");

            $file = fopen("/var/www/html/collaby/files/data/dados_minerador{$key}.csv", 'r');

            $first = TRUE;
            while(($line = fgetcsv($file, NULL, ",")) !== FALSE)
            {
                if ($first)
                {
                    $cabecalho = $line;
                    $first     = FALSE;
                }
                else
                {
                    $student_id = '';
                    if($line[1] == 'P')
                    {
                        $message    = new Message($line[2]);
                        $student_id = $message->student_id;
                        $group_id   = $message->group_id;
                    }
                    else
                    {
                        $answer = new Answer($line[2]);
                        $student_id = $answer->student_id;
                        $group_id   = $answer->message->group_id;
                    }

                    $process_data                 = new ProcessData();
                    $process_data->type           = $line[1];
                    $process_data->description    = $line[0];
                    $process_data->description_id = $line[2];
                    $process_data->date_process   = date('Y-m-d H:i:s');
                    $process_data->prob_0         = $line[3];
                    $process_data->prob_1         = $line[4];
                    $process_data->student_id     = $student_id;
                    $process_data->group_id       = $group_id;
                    $process_data->class_id       = $class->id;
                    $process_data->count_chars    = strlen($line[0]);
                    $process_data->store();
                }
            }
            // close the transaction
            TTransaction::close();

            // shows the success message
            new TMessage('info', 'Dados processados! Para ver o resultado acesse o dashboard.');
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public function onActive($param = NULL)
    {
        try
        {
            $data = $this->form->getData();

            // get the paramseter $key
            $key = $param['key'];
            // open a transaction with database
            TTransaction::open(self::$database);

            // instantiates object
            $object = new StudyClass($key, FALSE); 
            if($object->active)
            {
                $object->active = FALSE;
                $message = 'Turma encerrada!';
            }
            else
            {
                $object->active = TRUE;
                $message = 'Turma reaberta!';
            }

            // deletes the object from the database
            $object->store();

            // close the transaction
            TTransaction::close();

            $this->form->setData($data);

            // reload the listing
            $this->onReload( $param );
            // shows the success message
            new TMessage('info', $message);
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public function getTeacherName($teacher_id)
    {
        try
        {
            TTransaction::open('permission');

            $user = new SystemUsers($teacher_id);

            TTransaction::close();

            return $user->name;
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public function onDelete($param = null) 
    { 
        if(isset($param['delete']) && $param['delete'] == 1)
        {
            try
            {
                // get the paramseter $key
                $key = $param['key'];
                // open a transaction with database
                TTransaction::open(self::$database);

                // instantiates object
                $object = new StudyClass($key, FALSE); 

                // deletes the object from the database
                $object->delete();

                // close the transaction
                TTransaction::close();

                // reload the listing
                $this->onReload( $param );
                // shows the success message
                new TMessage('info', AdiantiCoreTranslator::translate('Record deleted'));
            }
            catch (Exception $e) // in case of exception
            {
                // shows the exception error message
                new TMessage('error', $e->getMessage());
                // undo all pending operations
                TTransaction::rollback();
            }
        }
        else
        {
            // define the delete action
            $action = new TAction(array($this, 'onDelete'));
            $action->setParameters($param); // pass the key paramseter ahead
            $action->setParameter('delete', 1);
            // shows a dialog to the user
            new TQuestion(AdiantiCoreTranslator::translate('Do you really want to delete ?'), $action);   
        }
    }

    public function onExportCsv($param = null) 
    {
        try
        {
            $this->onSearch();

            TTransaction::open(self::$database); // open a transaction
            $repository = new TRepository(self::$activeRecord); // creates a repository for Customer
            $criteria = $this->filter_criteria;

            if($filters = TSession::getValue(__CLASS__.'_filters'))
            {
                foreach ($filters as $filter) 
                {
                    $criteria->add($filter);       
                }
            }

            $records = $repository->load($criteria); // load the objects according to criteria
            if ($records)
            {
                $file = 'tmp/'.uniqid().'.csv';
                $handle = fopen($file, 'w');
                $columns = $this->datagrid->getColumns();

                $csvColumns = [];
                foreach($columns as $column)
                {
                    $csvColumns[] = $column->getLabel();
                }
                fputcsv($handle, $csvColumns, ';');

                foreach ($records as $record)
                {
                    $csvColumns = [];
                    foreach($columns as $column)
                    {
                        $name = $column->getName();
                        $csvColumns[] = $record->{$name};
                    }
                    fputcsv($handle, $csvColumns, ';');
                }
                fclose($handle);

                TPage::openFile($file);
            }
            else
            {
                new TMessage('info', _t('No records found'));       
            }

            TTransaction::close(); // close the transaction
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function onImportCsv($param = null) 
    {
        
    }

    

    /**
     * Register the filter in the session
     */
    public function onSearch()
    {
        // get the search form data
        $data = $this->form->getData();
        $filters = [];

        TSession::setValue(__CLASS__.'_filter_data', NULL);
        TSession::setValue(__CLASS__.'_filters', NULL);

        if (isset($data->id) AND ( (is_scalar($data->id) AND $data->id !== '') OR (is_array($data->id) AND (!empty($data->id)) )) )
        {

            $filters[] = new TFilter('id', '=', $data->id);// create the filter 
        }

        if (isset($data->name) AND ( (is_scalar($data->name) AND $data->name !== '') OR (is_array($data->name) AND (!empty($data->name)) )) )
        {

            $filters[] = new TFilter('name', 'like', "%{$data->name}%");// create the filter 
        }

        if (isset($data->institution_id) AND ( (is_scalar($data->institution_id) AND $data->institution_id !== '') OR (is_array($data->institution_id) AND (!empty($data->institution_id)) )) )
        {

            $filters[] = new TFilter('institution_id', '=', $data->institution_id);// create the filter 
        }

        $param = array();
        $param['offset']     = 0;
        $param['first_page'] = 1;

        // fill the form with data again
        $this->form->setData($data);

        // keep the search data in the session
        TSession::setValue(__CLASS__.'_filter_data', $data);
        TSession::setValue(__CLASS__.'_filters', $filters);

        $this->onReload($param);
    }

    /**
     * Load the datagrid with data
     */
    public function onReload($param = NULL)
    {
        try
        {
            // open a transaction with database 'collaby'
            TTransaction::open(self::$database);

            // creates a repository for Class
            $repository = new TRepository(self::$activeRecord);
            $limit = 20;

            $criteria = clone $this->filter_criteria;

            if (empty($param['order']))
            {
                $param['order'] = 'id';    
            }

            if (empty($param['direction']))
            {
                $param['direction'] = 'desc';
            }

            $criteria->setProperties($param); // order, offset
            $criteria->setProperty('limit', $limit);

            if($filters = TSession::getValue(__CLASS__.'_filters'))
            {
                foreach ($filters as $filter) 
                {
                    $criteria->add($filter);       
                }
            }

            $user_id = ApplicationService::getUserID();
            $groups = ApplicationService::getUserGroupsIDS();
            if(in_array(3, $groups))
            {
                $criteria->add(new TFilter('teacher_id', '=', $user_id));
            }

            // load the objects according to criteria
            $objects = $repository->load($criteria, FALSE);

            $this->datagrid->clear();
            if ($objects)
            {
                // iterate the collection of active records
                foreach ($objects as $object)
                {
                    // add the object inside the datagrid

                    $this->datagrid->addItem($object);

                }
            }

            // reset the criteria for record count
            $criteria->resetProperties();
            $count= $repository->count($criteria);

            $this->pageNavigation->setCount($count); // count of records
            $this->pageNavigation->setProperties($param); // order, page
            $this->pageNavigation->setLimit($limit); // limit

            // close the transaction
            TTransaction::close();
            $this->loaded = true;
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public function onShow($param = null)
    {

    }

    /**
     * method show()
     * Shows the page
     */
    public function show()
    {
        // check if the datagrid is already loaded
        if (!$this->loaded AND (!isset($_GET['method']) OR !(in_array($_GET['method'],  $this->showMethods))) )
        {
            if (func_num_args() > 0)
            {
                $this->onReload( func_get_arg(0) );
            }
            else
            {
                $this->onReload();
            }
        }
        parent::show();
    }

}

