<?php

class SystemPreferenceProcessDataForm extends TPage
{
    use Adianti\Base\AdiantiFileSaveTrait;

    protected $form;
    protected $container;
    protected $datagrid;
    private $formFields          = [];
    private static $database     = 'collaby';
    private static $activeRecord = 'preferences_process_data';
    private static $primaryKey   = 'id';
    private static $formName     = 'form_PreferencesProcessData';

    private static $students_list;
    private static $students_group_list;
    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct( $param )
    {
        parent::__construct();

        // creates the form
        $this->form = new BootstrapFormBuilder(self::$formName);
        // define the form title
        $this->form->setFormTitle("Configuração processamento de dados");

        $id                            = new THidden('id');
        $solicitude_percentage         = new TEntry('solicitude_percentage');
        $solicitude_message_percentage = new TEntry('solicitude_message_percentage');
        $participation_percentage      = new TEntry('participation_percentage');
        $relevance_percentage          = new TEntry('relevance_percentage');
        $avg_percentage                = new TEntry('avg_percentage');
        $training_base                 = new TFile('training_base');
        
        $solicitude_percentage->addValidation("Percentual de acurácia minima de classificação da mensagem", new TRequiredValidator()); 
        $solicitude_message_percentage->addValidation("Percentual de solicitude considerado pelo minerador", new TRequiredValidator()); 
        $participation_percentage->addValidation("Percentual de participação do aluno", new TRequiredValidator()); 
        $relevance_percentage->addValidation("Percentual de participações relevantes do aluno", new TRequiredValidator()); 
        $avg_percentage->addValidation("Percentual sobre o tamanho médio das mensagens dos alunos não solícitos", new TRequiredValidator()); 
        $training_base->addValidation("Base de treinamento", new TRequiredValidator()); 

        $id->setEditable(false);
        $solicitude_percentage->setMaxLength(100);
        $solicitude_message_percentage->setMaxLength(100);
        $participation_percentage->setMaxLength(100);
        $relevance_percentage->setMaxLength(100);
        $avg_percentage->setMaxLength(100);

        $id->setSize(100);
        $solicitude_percentage->setSize('100%');
        $solicitude_message_percentage->setSize('100%');
        $participation_percentage->setSize('100%');
        $relevance_percentage->setSize('100%');
        $avg_percentage->setSize('100%');
        $training_base->setSize('100%');

        $training_base->setAllowedExtensions(['csv']);
        
        // enable progress bar, preview
        $training_base->enableFileHandling();
        $training_base->enablePopover();

        $row        = $this->form->addFields([new TLabel("Percentual de acurácia mínima de classificação da mensagem:"), $solicitude_percentage, $id]);
        $row->style = ['col-sm-12'];

        $row        = $this->form->addFields([new TLabel("Percentual de solicitude considerado pelo minerador:"), $solicitude_message_percentage]);
        $row->style = ['col-sm-12'];

        $row        = $this->form->addFields([new TLabel("Percentual de participação do aluno:"), $participation_percentage]);
        $row->style = ['col-sm-12'];

        $row        = $this->form->addFields([new TLabel("Percentual de participações relevantes do aluno:"), $relevance_percentage]);
        $row->style = ['col-sm-12'];

        $row        = $this->form->addFields([new TLabel("Percentual sobre o tamanho médio das mensagens dos alunos não solícitos:"), $avg_percentage]);
        $row->style = ['col-sm-12'];

        $row        = $this->form->addFields([new TLabel("Base de treinamento:"), $training_base]);
        $row->style = ['col-sm-12'];

        // create the form actions
        $btn_onsave  = $this->form->addAction("Salvar", new TAction([$this, 'onSave']), 'fas:save #ffffff');
        $btn_onsave->addStyleClass('btn-primary'); 
        $btn_onclear = $this->form->addAction("Limpar formulário", new TAction([$this, 'onClear']), 'fas:eraser #dd5a43');

        // vertical box container
        $this->container        = new TVBox;
        $this->container->style = 'width: 100%';
        $this->container->class = 'form-container';
        $this->container->add($this->form);

        parent::add($this->container);
    }

    public function onSave($param = null) 
    {
        try
        {
            TTransaction::open(self::$database); // open a transaction

            $messageAction = null;

            $this->form->validate(); // validate form data

            $preferences = PreferencesProcessData::getObjects();
            if($preferences)
            {
                $preferences = $preferences[0];
                $object = new PreferencesProcessData($preferences->id);
            }
            else
            {
                // create an empty object 
                $object = new PreferencesProcessData();
            }

            $data = $this->form->getData(); // get form data as array

            $object->fromArray( (array) $data); // load the object with data

            $object->store(); // save the object 

            $this->saveFile($object, $data, 'training_base', 'files/base');

            // get the generated {PRIMARY_KEY}
            $data->id = $object->id; 

            $this->form->setData($data); // fill form data
            TTransaction::close(); // close the transaction

            new TMessage('info', "Registro salvo", $messageAction);

        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            $this->form->setData( $this->form->getData() ); // keep form data
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function onEdit( $param )
    {
        try
        {
            TTransaction::open(self::$database); // open a transaction

            $preferences = PreferencesProcessData::getObjects();
            if($preferences)
            {
                $preferences = $preferences[0];
                $object = new PreferencesProcessData($preferences->id);
                $this->form->setData($object); // fill the form 
            }
            else
            {
                // create an empty object 
                $object = new PreferencesProcessData();
            }

            TTransaction::close(); // close the transaction 
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    /**
     * Clear form data
     * @param $param Request
     */
    public function onClear( $param )
    {
        $this->form->clear(true);

    }
}