-- 2020-09-04
ALTER TABLE class ADD COLUMN teacher_id INTEGER NOT NULL;

ALTER TABLE message DROP COLUMN subject;

ALTER TABLE answer ADD COLUMN rating VARCHAR(20);

CREATE TABLE preference(
	id SERIAL NOT NULL, 
	user_id INT NOT NULL, 
	fl_filter_hidden BOOLEAN NOT NULL DEFAULT FALSE, 
	fl_menu_hiddenn BOOLEAN NOT NULL DEFAULT FALSE,
	PRIMARY KEY(id));

-- 2020-09-11
ALTER TABLE answer ADD COLUMN attachment TEXT;

--2020-09-15
ALTER TABLE activity ADD COLUMN active BOOLEAN;

--2020-09-25
ALTER TABLE message add column group_id integer references groups(id);

--2020-10-25
CREATE TABLE process_data(
	id SERIAL NOT NULL, 
	type VARCHAR(1) NOT NULL, 
	description TEXT NOT NULL, 
	description_id INT NOT NULL,
	date_process TIMESTAMP NOT NULL,
	prob_0 TEXT NOT NULL,
	prob_1 TEXT NOT NULL,
	student_id INT NOT NULL,
	PRIMARY KEY(id));

ALTER TABLE process_data ADD COLUMN group_id integer references groups(id);

ALTER TABLE message DROP COLUMN student_group_id;

--2021-06-28
ALTER TABLE class ADD COLUMN active BOOLEAN;

--2021-06-30
ALTER TABLE student_class ADD COLUMN grade_ini NUMERIC(10,2);
ALTER TABLE student_class ADD COLUMN grade_end NUMERIC(10,2);

--2021-07-06
ALTER TABLE student_class ADD COLUMN role VARCHAR(1);

--2021-09-06
CREATE TABLE preferences_process_data(
	id SERIAL NOT NULL,
	solicitude_percentage INTEGER NOT NULL,
	solicitude_message_percentage INTEGER NOT NULL,
	participation_percentage INTEGER NOT NULL,
	relevance_percentage INTEGER NOT NULL,
	training_base TEXT,
	PRIMARY KEY(id)
);

ALTER TABLE message ADD COLUMN count_chars INTEGER;
ALTER TABLE answer ADD COLUMN count_chars INTEGER;

--2021-09-08
ALTER TABLE process_data ADD COLUMN class_id INTEGER REFERENCES class(id);
ALTER TABLE process_data ADD COLUMN count_chars INTEGER;

ALTER TABLE message ADD COLUMN rating VARCHAR(20);

--2022-01-24
ALTER TABLE answer alter column rating set default 'conversa_social';
ALTER TABLE message alter column rating set default 'conversa_social';

--2022-01-27
ALTER TABLE preferences_process_data ADD COLUMN avg_percentage INTEGER;