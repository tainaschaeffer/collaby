<?php

use Adianti\Control\TAction;
use Adianti\Database\TRepository;
use Adianti\Database\TTransaction;
use Adianti\Widget\Form\TCombo;

class DiscussionForm extends TPage
{
    protected $form;
    private static $database = 'collaby';
    private static $formName = 'form_Message';

    private $fab;
    private $container;
    private $array_answers;
    private $student_id;

    private $rate_icon = 'comments-alt';

    const ENTRY_PARTICIPATE = array('group_id', 'activity_id');

    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct( $param )
    {
        parent::__construct();

        // creates the form
        $this->form = new BootstrapFormBuilder(self::$formName);
        // define the form title
        $this->form->setFormTitle("Discussão");

        TTransaction::open(self::$database);

        $student_id = ApplicationService::getUserID();
        $groups = ApplicationService::getUserGroupsIDS();

        $rate_icon = Message::RATING_ICON[1];
        
        if(isset($param['id']) && $param['id'])
        {
            $data_message = Message::find($param['id']);

            if($data_message->rating)
            {
                $key_rating  = array_search($data_message->rating, Message::RATING);

                if(!array_key_exists($key_rating, Message::RATING_ICON))
                {
                    $key_rating = 1;
                }

                $rate_icon = Message::RATING_ICON[$key_rating];
            }
            else
            {
                $rate_icon = Message::RATING_ICON[1];
            }

            $student_name = $data_message->getStudentName($param['id']);
        }
        
        TTransaction::close();
        
        $criteriaActivity = new TCriteria;
        $criteriaActivity->add(new TFilter('id', 'IN', "(SELECT activity.id 
                                                           FROM activity, 
                                                                groups, 
                                                                student_group,
                                                                class
                                                          WHERE activity.class_id = groups.class_id 
                                                            AND student_group.group_id = groups.id 
                                                            AND groups.class_id = class.id
                                                            AND activity.active IS TRUE
                                                            AND class.active IS TRUE
                                                            AND student_group.student_id = {$student_id})"));

        $criteriaGroup = new TCriteria;
        $criteriaGroup->add(new TFilter('id', 'IN', "(SELECT student_group.group_id 
                                                        FROM student_group,
                                                             student_class,
                                                             class 
                                                       WHERE student_group.student_id = student_class.student_id
                                                         AND student_class.class_id = class.id
                                                         AND student_group.student_id = {$student_id}
                                                         AND class.active IS TRUE)"));
                                                         
        $id          = new THidden('id');
        $message     = new TText('message');
        $group_id    = new TDBCombo('group_id', 'collaby', 'Group', 'id', '{name} - {class->name}','id asc', $criteriaGroup);
        $group_id->setChangeAction(new TAction([$this, 'onChangeGroup']));

        $activity_id = new TDBCombo('activity_id', 'collaby', 'Activity', 'id', '{name} - {class->name}','id desc', $criteriaActivity);

        $message->addValidation("Mensagem", new TRequiredValidator());
        $group_id->addValidation("Grupo", new TRequiredValidator());
        $activity_id->addValidation("Atividade", new TRequiredValidator());

        $message->setSize('100%', 100);
        $group_id->setSize('100%');
        $activity_id->setSize('100%');

        $group_id->setDefaultOption(FALSE);
        $activity_id->setDefaultOption(FALSE);

        $row        = $this->form->addFields([new TLabel("Grupo:"), $group_id], [new TLabel("Atividade:"), $activity_id]);
        $row->style = ['col-sm-6', 'col-sm-6'];
        $row->style = 'display: none;';
        $row->class = 'participate';

        $row        = $this->form->addFields([new TLabel("<b>{$student_name}:</b>"), $message, $id]);
        $row->style = ['col-sm-12'];

        // create the form actions
        $btn_onsave = $this->form->addAction("Salvar", new TAction([$this, 'onSave']), 'fas:save #ffffff');
        $btn_onsave->addStyleClass('btn-primary'); 

        $btn_onclear = $this->form->addAction("Limpar formulário", new TAction([$this, 'onClear']), 'fas:eraser #dd5a43');

        $btn_onback = $this->form->addAction("Voltar para discussões", new TAction(['DiscussionList', 'onShow']), 'fas:angle-left #dd5a43');

        if(isset($param['id']))
        {
            $btn_onView = $this->form->addAction("Detalhes", new TAction([$this, 'onView'], ['id' => isset($param['id'])??NULL]), 'fa:search blue #dd5a43');
        }

        if(in_array(3, $groups))
        {
            $btn_onrate = $this->form->addAction("", new TAction([$this, 'onRate'], ['key' => $param['id']??NULL, 'type' => 'message']), "fas:{$rate_icon} #dd5a43");
        }
        // vertical box container
        $this->container = new TVBox;
        $this->container->style = 'width: 100%';
        $this->container->class = 'form-container';
        $this->container->add($this->form);

        parent::add($this->container);
    }

    public static function onChangeGroup($param)
    {
        try
        {
            TTransaction::open(self::$database);

            $group_id = $param['group_id'];

            $criteriaActivity = new TCriteria;
            $criteriaActivity->add(new TFilter('id', 'IN', "(SELECT activity.id 
                                                            FROM activity, 
                                                                    groups, 
                                                                    student_group,
                                                                    class
                                                            WHERE activity.class_id = groups.class_id 
                                                                AND student_group.group_id = groups.id 
                                                                AND groups.class_id = class.id
                                                                AND activity.active IS TRUE
                                                                AND class.active IS TRUE
                                                                AND student_group.group_id = {$group_id})"));

            
            $repository = new TRepository('Activity');
            $activities = $repository->load($criteriaActivity,FALSE); 
            
            $itens = []; 
            foreach ($activities as $activity) 
            { 
                $itens[$activity->id] = $activity->name.' - '.$activity->class->name;
            }

            TTransaction::close();

            TCombo::reload(self::$formName, 'activity_id', $itens);
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public static function onView($param)
    {
        try
        {
            TTransaction::open(self::$database);

            $message_id = $param['id'];
            $message    = new Message($message_id);

            $info  = '<b>Turma:</b><br>'.$message->group->class->name . '<br>';
            $info .= '<b>Grupo:</b><br>'.$message->group->name . '<br>';
            $info .= '<b>Atividade:</b><br>'.$message->activity->name.'<br>';
            $info .= '<b>Autor:</b><br>'.Message::getStudentName($message_id);

            TTransaction::close();
            
            $window = TWindow::create('Dados da mensagem', 0.7, 0.4);
            $window->add($info);
            $window->show();
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public function onSave($param = null) 
    {
        try
        {
            TTransaction::open(self::$database); // open a transaction
            $messageAction = null;

            $this->form->validate(); // validate form data

            $object = new Message(); // create an empty object 

            $student_id = SystemUsers::newFromLogin(TSession::getValue('login'))->id;

            $data               = $this->form->getData(); // get form data as array

            $data->date_message = date('Y-m-d H:i:s');
            $data->student_id   = $student_id;
            $data->count_chars  = strlen($data->message);
            
            $object->fromArray( (array) $data); // load the object with data

            $object->store(); // save the object 

            // get the generated {PRIMARY_KEY}
            $data->id = $object->id; 

            $this->form->setData($data); // fill form data

            $this->makeTimeline($data);
            $this->makeFab($data);
            
            TTransaction::close(); // close the transaction

            new TMessage('info', "Registro salvo", $messageAction); 
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            $this->form->setData( $this->form->getData() ); // keep form data
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function onEdit( $param )
    {
        try
        {
            if (isset($param['key']))
            {
                self::showEntry(TRUE);

                TScript::create("$('.card-header').show();
                                 $('.participate').show();
                                 $('#tbutton_btn_limpar_formulario').show();
                                 $('#tbutton_btn_salvar').show();");

                $key = $param['key'];  // get the parameter $key

                TTransaction::open(self::$database); // open a transaction

                $object = new Message($key); // instantiates the Active Record 

                $this->form->setData($object); // fill the form 

                $this->makeTimeline($object);
                $this->makeFab($object);

                TTransaction::close(); // close the transaction 
            }
            else
            {
                $this->form->clear();

                TScript::create("$('.card-header').show();
                                 $('.participate').show();
                                 $('#tbutton_btn_limpar_formulario').show();
                                 $('#tbutton_btn_salvar').show();");
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    private static function showEntry($fl_show = FALSE)
    {
        if(self::ENTRY_PARTICIPATE)
        {
            foreach(self::ENTRY_PARTICIPATE as $entry)
            {
                if($fl_show)
                {
                    BootstrapFormBuilder::showField(self::$formName, $entry);
                }
                else
                {
                    BootstrapFormBuilder::hideField(self::$formName, $entry);
                }
            }
        }
    }

    public function onParticipate( $param )
    {
        try
        {
            if (isset($param['key']))
            {
                self::showEntry(FALSE);
                TScript::create("$('.card-header').hide();
                                 $('#tbutton_btn_limpar_formulario').hide();
                                 $('#tbutton_btn_salvar').hide();");

                $key = $param['key'];  // get the parameter $key

                TTransaction::open(self::$database); // open a transaction

                $object = new Message($key); // instantiates the Active Record 

                $this->form->setData($object); // fill the form 

                $this->makeTimeline($object);
                $this->makeFab($object);

                TTransaction::close(); // close the transaction 
            }
            else
            {
                $this->form->clear();
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function makeFab($dados)
    {
        $action = new TAction(['AnswerForm', 'onLoad'], ['message_id'       => $dados->id,
                                                         'student_id'       => $this->student_id,
                                                         'load_discussions' => TRUE]);

        $this->fab = new TFabButton('my_fab','#29B6F6','fas:plus #fff', 'Ações', $action);

        //$this->fab->addItem('Enviar resposta', $action, 'fas:comments', '#FFF');
        $this->container->add($this->fab);
    }

    public function makeTimeline($message)
    {
        try
        {
            TTransaction::open(self::$database); // open a transaction

            $criteria = new TCriteria;
            $criteria->add(new TFilter('message_id', '=', $message->id));
            $criteria->setProperty('order', 'date_answer desc');

            $answers = Answer::getObjects($criteria); // instantiates the Active Record

            TTransaction::close(); // close the transaction 

            $this->student_id = SystemUsers::newFromLogin(TSession::getValue('login'))->id;

            TTransaction::open('permission');
                
            $user = new SystemUsers($this->student_id);

            $groups = $user->getSystemUserGroupIds();
            $display_rate = FALSE;
            if(in_array(SystemGroup::TEACHER, $groups))
            {
                $display_rate = TRUE;
            }

            TTransaction::close();

            $this->array_answers = [];
            if($answers)
            {
                $timeline = new TTimeline;

                foreach ($answers as $answer) 
                {
                    $position = 'center';

                    if($answer->student_id == $message->student_id)
                    {
                        $icon = 'fa:arrow-left bg-green';
                    }
                    else
                    {
                        $icon = 'fa:arrow-right bg-blue';
                        //$position = 'right';
                    }

                    if($answer->student_id == $this->student_id)
                    {
                        $this->array_answers[$answer->id] = $answer->id;
                    }

                    $student_answer = $answer->answer;

                    if($answer->attachment)
                    {
                        $button               = new TElement('a');
                        $button->{'href'}     = "download.php?file=files/answer/$answer->id/$answer->attachment";
                        $button->{'generate'} = "adianti";
                        $button->{'class'}    = 'btn btn-default';
                        $button->add('Ver anexo');

                        $student_answer .= "<br><br> {$button} <br>";
                    }
                    
                    if($answer->rating)
                    {
                        $key_rating  = array_search($answer->rating, Answer::RATING);

                        if(array_key_exists($key_rating, Answer::RATING_ICON))
                        {
                            $key_rating = $key_rating;
                        }
                        else
                        {
                            $key_rating = 1;
                        }

                        $this->rate_icon = Answer::RATING_ICON[$key_rating];
                    }
                    else
                    {
                        $this->rate_icon = Answer::RATING_ICON[1];
                    }

                    $i                = new TElement('i');
                    $i->{'class'}     = "fas fa-{$this->rate_icon} #555";
                    $i->{'style'}    = "";

                    $btn              = new TElement('a');

                    if($display_rate)
                    {
                        $btn->{'onclick'} = "__adianti_load_page('index.php?class=DiscussionForm&method=onRate&key={$answer->id}&type=answer');";
                    }

                    $btn->{'class'}   = 'btn btn-default btn-sm';
                    $btn->add($i);

                    $student_answer .= " <br> $btn";

                    $timeline->addItem($answer->id, "{$answer->student_name}",  "{$student_answer}", "{$answer->date_answer}",  $icon,  $position,  $answer);
                    //$timeline->{'align'} = "center";
                }

                $display_condition = function( $object = false ) 
                {
                    if(array_key_exists($object->id, $this->array_answers))
                    {
                        return true;
                    }

                    return false;
                };
                
                $action_onEditAnswer   = new TAction(['AnswerForm', 'onEdit'], ['key' => '{id}']);
                $action_onDeleteAnswer = new TAction([$this, 'onDeleteAnswer'], ['key' => '{id}']);
                $action_onRate         = new TAction([$this, 'onRate'], ['key' => '{id}', 'type' => 'answer']);
                
                $action_onEditAnswer->setProperty('btn-class', 'btn btn-primary');
                $action_onDeleteAnswer->setProperty('btn-class', 'btn btn-danger');
                $action_onRate->setProperty('btn-class', 'btn');
                
                $timeline->addAction($action_onEditAnswer, '', 'far:edit #fff', $display_condition);
                $timeline->addAction($action_onDeleteAnswer, '', 'fas:trash-alt #fff', $display_condition);

                $timeline->setUseBothSides();
                $timeline->setTimeDisplayMask('dd/mm/yyyy');
                $timeline->setFinalIcon( 'fa:genderless bg-red' );

                parent::add($timeline);
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function onRate($param)
    {
        try
        {
            if (isset($param['key']))
            {
                $key = $param['key'];  // get the parameter $key

                TTransaction::open(self::$database); // open a transaction

                if($param['type'] == 'answer')
                {
                    $object      = new Answer($key); // instantiates the Active Record 
                    $rating      = Answer::RATING;
                    $rating_icon = Answer::RATING_ICON;
                }
                else
                {
                    $object      = new Message($key); // instantiates the Active Record 
                    $rating      = Message::RATING;
                    $rating_icon = Message::RATING_ICON;
                }

                if($object->rating)
                {
                    $key_rating  = array_search($object->rating, $rating);

                    if(array_key_exists($key_rating+1, $rating))
                    {
                        $key_rating = $key_rating+1;
                    }
                    else
                    {
                        $key_rating = 0;
                    }

                    $next_rating = $rating[$key_rating];
                    $next_rating = explode(' ', $next_rating);

                    $object->rating = $next_rating[0];

                    $this->rate_icon = $rating_icon[$key_rating];
                }
                else
                {
                    $rating = $rating[1];
                    $rating = explode(' ', $rating);

                    $object->rating = $rating[0];

                    $this->rate_icon = $rating_icon[1];
                }

                $object->store();

                $message_id = isset($object->message_id)? $object->message_id: $object->id;

                //$this->onParticipate(['key' => $message_id]);
                
                TScript::create("__adianti_load_page('index.php?class=DiscussionForm&method=onParticipate&key={$message_id}&id={$message_id}')");

                TTransaction::close(); // close the transaction 
            }
            else
            {
                $this->form->clear();
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function onDeleteAnswer($param = NULL)
    {
        if(isset($param['delete']) && $param['delete'] == 1)
        {
            try
            {
                // get the paramseter $key
                $key = $param['key'];
                // open a transaction with database
                TTransaction::open(self::$database);

                // instantiates object
                $object = new Answer($key, FALSE); 

                $message_id = $object->message_id;

                // deletes the object from the database
                $object->delete();

                // close the transaction
                TTransaction::close();

                $this->onParticipate(['key' => $message_id]);

                // shows the success message
                new TMessage('info', AdiantiCoreTranslator::translate('Record deleted'));
            }
            catch (Exception $e) // in case of exception
            {
                // shows the exception error message
                new TMessage('error', $e->getMessage());
                // undo all pending operations
                TTransaction::rollback();
            }
        }
        else
        {
            // define the delete action
            $action = new TAction(array($this, 'onDeleteAnswer'));
            $action->setParameters($param); // pass the key paramseter ahead
            $action->setParameter('delete', 1);
            // shows a dialog to the user
            new TQuestion(AdiantiCoreTranslator::translate('Do you really want to delete ?'), $action);   
        }
        
    }

    /**
     * Clear form data
     * @param $param Request
     */
    public function onClear( $param )
    {
        $this->form->clear(true);

        TScript::create("$('.card-header').show();
                                 $('.participate').show();
                                 $('#tbutton_btn_limpar_formulario').show();
                                 $('#tbutton_btn_salvar').show();");
    }

    public function onShow($param = null)
    {
    } 

}

