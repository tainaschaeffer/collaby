<?php
/**
 * TFabButton
 * Date: 17-05-2016
 *
 * @author Jackson D. Majolo
 */
class TFabButton extends TElement
{
	private $id;
	private $title;
	private $name;
	private $icon;
	private $background;
	private $action;
	private $items = [];

	public function __construct($name, $background = NULL, $icon = NULL, $title = '', $action = NULL)
	{
		parent::__construct($name);

		$this->title 	  = $title;
		$this->name       = $name;
		$this->icon       = $icon;
		$this->background = $background;
		$this->action 	  = $action;
		$this->id         = 'tfabbutton'.mt_rand(1000000000, 1999999999);
	}

	public function setIcon( $icon )
	{
		$this->icon = $icon;
	}

	public function setBackground( $background )
	{
		$this->background = $background;
	}

	public function addItem($title, TAction $action, $icon, $color)
	{
		$item = new stdClass;
		$item->{'title'}   = $title;
		$item->{'action'}  = $action;
		$item->{'icon'}    = $icon;
		$item->{'color'}   = $color;

		$this->items[] = $item;
	}

	public function getItems()
	{
		return $this->items;
	}

	private function makeButton($item)
	{
		$button = new TElement('li');
		$link   = new TElement('button');
		$image  = new TImage($item->icon);

		$link->{'class'}        = 'tfab-buttons__link';
		$link->{'style'}        = "background-color:{$item->color}";
		$link->{'data-tooltip'} = $item->title;
		$link->{'aria-label'} 	= $item->title;
		$link->add($image);

		$button->{'class'}   = 'tfab-buttons__item';
		$button->{'onclick'} = "__adianti_load_page2('{$item->action->serialize()}');";
		$button->add($link);

		return $button;
	}

	private function makeFab()
	{
		$fab = new TElement('button');

		$fab->{'class'} = 'tfab-action-button';
		$fab->{'style'} = "background-color:{$this->background}";

		if($this->title)
		{
			$fab->{'aria-label'} = $this->title;
		}

		if($this->action)
		{
			$fab->{'onclick'} = "__adianti_load_page2('{$this->action->serialize()}');";
		}

		$image = new TImage($this->icon);
		$fab->add( $image );

		return $fab;
	}

	public function show()
	{
		$content = new TElement('div');
		$content->{'class'} = 'tfab';

		$fab = $this->makeFab();

		$content->add($fab);

		$buttons = new TElement('ul');
		$buttons->{'class'} = 'tfab-buttons';

		if(!empty($this->getItems()))
		{
			foreach($this->getItems() as $item)
			{
				$buttons->add($this->makeButton($item));
			}
		}

		$content->add($buttons);

		$content->show();
	}
}

