import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import nltk
import re
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from nltk.corpus import stopwords
from pprint import pprint
from sklearn.datasets import fetch_20newsgroups
from nltk.stem.snowball import SnowballStemmer
import csv
import sys

csv_training_base = sys.argv[1]
csv_class_base = sys.argv[2]
csv_process_data = sys.argv[3]

nltk.download('rslp')

training_base = pd.read_csv('/var/www/html/collaby/' + csv_training_base, encoding='utf-8', delimiter=';');

example = pd.DataFrame(training_base)
example.columns = ['Frase', 'Sentimento', 'Aluno']

test_base = [('', '', '')];

nltk.download('stopwords')
list_stopwords = nltk.corpus.stopwords.words('portuguese')
np.transpose(list_stopwords)

def removeStopwords(text):
  phrases = []
  for(words, feeling, student) in text:
    noStop = [p for p in words.split() if p not in list_stopwords]
    phrases.append(noStop, feeling, student)
  return phrases

def aply_Stemmer(text):
  stemmer = nltk.stem.RSLPStemmer()
  phrases_without_Stemming = []
  for(words, sentimento, student) in text:
    with_Stemming = [str(stemmer.stem(p)) for p in words.split() if p not in list_stopwords]
    phrases_without_Stemming.append((with_Stemming, sentimento, student))
  return phrases_without_Stemming

tuples = [tuple(x) for x in training_base.values]
phrases_with_Stem_training = aply_Stemmer(tuples)
phrases_with_Stem_test = aply_Stemmer(test_base)

def search_words(phrases):
  all_words = []
  for(words, sentimento, student) in phrases:
    all_words.extend(words)
  return all_words

words_training = search_words(phrases_with_Stem_training)
words_test = search_words(phrases_with_Stem_test)

def search_frequency(words):
  words = nltk.FreqDist(words)
  return words

frequency_training = search_frequency(words_training)
frequency_test = search_frequency(words_test)

def search_single_words(frequency):
  freq = frequency.keys()
  return freq

single_words_training = search_single_words(frequency_training)
single_words_test = search_single_words(frequency_test)

def words_extractor(document):
  doc = set(document)
  characteristics = {}
  for words in single_words_training:
    characteristics['%s' % words] = (words in doc)
  return characteristics

def words_extractor_test(document):
  doc = set(document)
  characteristics = {}
  for words in single_words_test:
    characteristics['%s' % words] = (words in doc)
  return characteristics

base_completa_treinamento = nltk.classify.apply_features(words_extractor, phrases_with_Stem_training)
#base_completa_teste = nltk.classify.apply_features(words_extractor_test, phrases_with_Stem_test)

classifier = nltk.NaiveBayesClassifier.train(base_completa_treinamento)

process_base = pd.read_csv(csv_class_base, encoding='utf-8', delimiter=';');
process = [tuple(x) for x in process_base.values]

f = open(csv_process_data, 'w', newline='', encoding='utf-8')
w = csv.writer(f)
w.writerow(['Texto', 'Tipo', 'Texto Id', 'Prob 0', 'Prob 1'])

for n in process:
  test = n[0];
  #print(test);
  #print(n[2]);
  
  testStemming = []
  stemmer = nltk.stem.RSLPStemmer()
  for(words_training) in test.split():
    withStem = [p for p in words_training.split()]
    testStemming.append(str(stemmer.stem(withStem[0])))

  new = words_extractor(testStemming)

  distribution = classifier.prob_classify(new)
  w.writerow([n[0], n[1], n[2], distribution.prob(0), distribution.prob(1)])