<?php

class AnswerForm extends TWindow
{
    use Adianti\Base\AdiantiFileSaveTrait;

    protected $form; // form
    private static $database = 'collaby';
    private static $formName = 'form_Answer';

    /**
     * Class constructor
     * Creates the page
     */
    function __construct($param)
    {
        parent::__construct();
        parent::setSize(0.9, 0.9);
        parent::setTitle('Enviar resposta');

        // creates the form
        $this->form = new BootstrapFormBuilder(self::$formName);

        $id               = new THidden('id');
        $answer           = new TText('answer');
        $message_id       = new THidden('message_id');
        $student_id       = new THidden('student_id');
        $load_discussions = new THidden('load_discussions');
        //$attachment = new TFile('attachment');

        //$attachment->setAllowedExtensions(['png', 'jpg', 'pdf']);
        //$attachment->enableFileHandling();

        $answer->addValidation("Resposta", new TRequiredValidator()); 

        $answer->setSize('100%', 170);
        //$attachment->setSize('100%');

        $row        = $this->form->addFields([new TLabel("Resposta:"), $answer]);
        $row->style = ['col-sm-12'];

        //$row        = $this->form->addFields([new TLabel("Anexo:"), $attachment]);
        //$row->style = ['col-sm-12'];

        $row        = $this->form->addFields(['', $id], ['', $message_id], ['', $student_id], ['', $load_discussions]);
        $row->style = ['col-sm-2', 'col-sm-4', 'col-sm-3', 'col-sm-3'];

        // create the form actions
        $btn_onsave = $this->form->addAction("Salvar", new TAction([$this, 'onSave']), 'fas:save #ffffff');
        $btn_onsave->addStyleClass('btn-primary'); 

        // vertical box container
        $this->container = new TVBox;
        $this->container->style = 'width: 100%';
        $this->container->class = 'form-container';
        //$this->container->add(new TXMLBreadCrumb('menu.xml', 'DiscussionList'));
        $this->container->add($this->form);

        parent::add($this->container);
    }

    public function onLoad($param)
    {
        try
        {
            $data                   = new stdClass;
            $data->message_id       = $param['message_id'];
            $data->student_id       = $param['student_id'];
            $data->load_discussions = $param['load_discussions'];

            TForm::sendData(self::$formName, $data);
        }
        catch(Exception $e)
        {
            TTransaction::rollback();
            new TMessage('error', $e->getMessage());
            SystemErrorLog::write($e);
            parent::closeWindow();
        }
    }

    public function onEdit( $param )
    {
        try
        {
            if (isset($param['key']))
            {
                $key = $param['key'];  // get the parameter $key
                TTransaction::open(self::$database); // open a transaction

                $object = new Answer($key); // instantiates the Active Record 

                $this->form->setData($object); // fill the form 

                TTransaction::close(); // close the transaction 
            }
            else
            {
                $this->form->clear();
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }
    
    public function onSave($param = NULL)
    {
        try
        {
            TTransaction::open(self::$database); // open a transaction
            $this->form->validate(); // validate form data

            $object = new Answer(); // create an empty object 

            $data               = $this->form->getData(); // get form data as array
            $data->date_answer  = date('Y-m-d H:i:s');
            $data->message_id   = $data->message_id;
            $data->student_id   = $data->student_id;
            $data->count_chars  = strlen($data->answer);

            $object->fromArray( (array) $data); // load the object with data

            $object->store(); // save the object 

            //$this->saveFile($object, $data, 'attachment', "files/answer/");

            // get the generated {PRIMARY_KEY}
            $data->id = $object->id; 

            $this->form->setData($data); // fill form data

            new TMessage('info', "Resposta enviada"); 

            if($data->load_discussions)
            {
                TApplication::loadPage('DiscussionForm', 'onParticipate', ['key' => $data->message_id]);
            }
            else
            {
                TApplication::loadPage('DiscussionList', 'onReload');
            }
     
            TTransaction::close(); // close the transaction
        }
        catch (Exception $e) // in case of exception
        {
            TTransaction::rollback(); // undo all pending operations
        }
    }
    
    public function onShow($param = null)
    {
    }
}