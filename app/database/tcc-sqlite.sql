PRAGMA foreign_keys=OFF; 

CREATE TABLE activity( 
      id  INTEGER    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
      description text   NOT NULL  , 
      class_id int   NOT NULL  , 
 PRIMARY KEY (id),
FOREIGN KEY(class_id) REFERENCES class(id)) ; 

CREATE TABLE class( 
      id  INTEGER    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
      institution_id int   NOT NULL  , 
 PRIMARY KEY (id),
FOREIGN KEY(institution_id) REFERENCES institution(id)) ; 

CREATE TABLE group( 
      id  INTEGER    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
      class_id int   NOT NULL  , 
 PRIMARY KEY (id),
FOREIGN KEY(class_id) REFERENCES class(id)) ; 

CREATE TABLE institution( 
      id  INTEGER    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
 PRIMARY KEY (id)) ; 

CREATE TABLE message( 
      id  INTEGER    NOT NULL  , 
      subject varchar  (100)   NOT NULL  , 
      message text   NOT NULL  , 
      student_class_id int   NOT NULL  , 
      activity_id int   NOT NULL  , 
 PRIMARY KEY (id),
FOREIGN KEY(student_class_id) REFERENCES student_class(id),
FOREIGN KEY(activity_id) REFERENCES activity(id)) ; 

CREATE TABLE student_class( 
      id  INTEGER    NOT NULL  , 
      class_id int   NOT NULL  , 
      student_id int   NOT NULL  , 
 PRIMARY KEY (id),
FOREIGN KEY(class_id) REFERENCES class(id)) ; 

 
 
  
