<?php

class PreferencesProcessData extends TRecord
{
    const TABLENAME  = 'preferences_process_data';
    const PRIMARYKEY = 'id';
    const IDPOLICY   = 'serial'; // {max, serial}

    private $class;

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('solicitude_percentage');
        parent::addAttribute('solicitude_message_percentage');
        parent::addAttribute('participation_percentage');
        parent::addAttribute('relevance_percentage');
        parent::addAttribute('avg_percentage');
        parent::addAttribute('training_base');
    }
}

