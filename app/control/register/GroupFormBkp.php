<?php

class GroupForm extends TPage
{
    protected $form;
    private $formFields          = [];
    private static $database     = 'collaby';
    private static $activeRecord = 'Group';
    private static $primaryKey   = 'id';
    private static $formName     = 'form_Group';

    private static $students_list;
    private static $students_group_list;
    /**
     * Form constructor
     * @param $param Request
     */
    public function __construct( $param )
    {
        parent::__construct();

        // creates the form
        $this->form = new BootstrapFormBuilder(self::$formName);
        // define the form title
        $this->form->setFormTitle("Cadastro de grupo");

        $id       = new TEntry('id');
        $name     = new TEntry('name');
        $class_id = new TDBCombo('class_id', 'collaby', 'StudyClass', 'id', '{name}','id asc'  );        

        $name->addValidation("Nome", new TRequiredValidator()); 
        $class_id->addValidation("Turma", new TRequiredValidator()); 

        $name->setMaxLength(45);
        $id->setEditable(false);

        $id->setSize(100);
        $name->setSize('100%');
        $class_id->setSize('100%');

        $row        = $this->form->addFields([new TLabel("Código:"), $id]);
        $row->style = ['col-sm-12'];

        $row        = $this->form->addFields([new TLabel("Nome:"), $name]);
        $row->style = ['col-sm-12'];

        $row        = $this->form->addFields([new TLabel("Turma:"), $class_id]);
        $row->style = ['col-sm-12'];

        // create the form actions
        $btn_onsave  = $this->form->addAction("Salvar", new TAction([$this, 'onSave']), 'fas:save #ffffff');
        $btn_onsave->addStyleClass('btn-primary'); 
        $btn_onclear = $this->form->addAction("Limpar formulário", new TAction([$this, 'onClear']), 'fas:eraser #dd5a43');
        $btn_onsearch = $this->form->addAction('Voltar', new TAction(['GroupList', 'onShow']), 'fa:table #07488c');
        $btn_onsearch->addStyleClass('btn-default');

        // vertical box container
        $container        = new TVBox;
        $container->style = 'width: 100%';
        $container->class = 'form-container';
        $container->add(new TXMLBreadCrumb('menu.xml', 'GroupList'));
        $container->add($this->form);

        parent::add($container);
    }

    public function onSave($param = null) 
    {
        try
        {
            TTransaction::open(self::$database); // open a transaction

            $messageAction = null;

            $this->form->validate(); // validate form data

            $object = new Group(); // create an empty object 

            $data = $this->form->getData(); // get form data as array
            $object->fromArray( (array) $data); // load the object with data

            $object->store(); // save the object 

            // get the generated {PRIMARY_KEY}
            $data->id = $object->id; 

            StudentGroup::where('group_id', '=', $data->id)->delete();
            if(isset($param['students_group_list']))
            {
                foreach ($param['students_group_list'] as $student) 
                {
                    $object_student             = new StudentGroup();
                    $object_student->student_id = $student;
                    $object_student->group_id   = $data->id;
                    $object_student->store();                    
                }
            }

            $this->form->setData($data); // fill form data
            //self::onChangeClass(['class_id' => $object->class_id, 'id' => $object->id]);

            TTransaction::close(); // close the transaction

            new TMessage('info', "Registro salvo", $messageAction);

            $students_list       = new TSortList('students_list');
            $students_group_list = new TSortList('students_group_list');
                    
            $students_list->setSize(400, 400);
            $students_group_list->setSize(400, 400);
                    
            $students_list->connectTo($students_group_list);
            $students_group_list->connectTo($students_list);

            $class_id = $object->class_id;
            $group_id = $object->id;

            $selected_students  = [];
            $available_students = [];

            TTransaction::open('collaby');

            if(!empty($class_id))
            {
                $students_class = StudentClass::getAvailableStudentByClass($class_id);
                
                if($students_class)
                {
                    foreach ($students_class as $student_class) 
                    {
                        $system_user = new SystemUsers($student_class->student_id);
                        $available_students[$student_class->student_id] = $system_user->name;
                    }
                }

                if($group_id)
                {
                    $students = StudentGroup::getStudentsByGroup($group_id);

                    if($students)
                    {
                        foreach ($students as $student) 
                        {
                            $system_user = new SystemUsers($student->student_id);
                            $selected_students[$student->student_id] = $system_user->name;       
                        }
                    }
                }

                if(count($available_students) > 0)
                {
                    $students_list->addItems($available_students);
                }

                if(count($selected_students) > 0)
                {
                    $students_group_list->addItems($selected_students);
                }
            }

            TTransaction::close();

            $row        = $this->form->addFields([$students_list, $students_group_list]);
            $row->style = ['col-sm-6', 'col-sm-6'];
            $row->id    = 'tsortlist'; 
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            $this->form->setData( $this->form->getData() ); // keep form data
            TTransaction::rollback(); // undo all pending operations
        }
    }

    public function onEdit( $param )
    {
        try
        {
            if (isset($param['key']))
            {
                $key = $param['key'];  // get the parameter $key
                TTransaction::open(self::$database); // open a transaction

                $object = new Group($key); // instantiates the Active Record 

                $this->form->setData($object); // fill the form 

                TTransaction::close(); // close the transaction 
            }
            else
            {
                $this->form->clear();
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }

    /**
     * Clear form data
     * @param $param Request
     */
    public function onClear( $param )
    {
        $this->form->clear(true);

    }

    public function onShow($param = null)
    {

    } 

}

