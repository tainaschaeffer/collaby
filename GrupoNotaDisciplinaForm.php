<?php
use Adianti\Control\TPage;
use Adianti\Widget\Form\TRadioGroup;
class GrupoNotaDisciplinaForm extends TPage
{
    protected $form;
    private $template;
    private $criteria;
    private static $formName = 'form_AcdGrupoNotaDisciplina';
    public function __construct( $param )
    {
        parent::__construct();
        parent::setPageTitle("Grupo de notas da disciplina");
        $this->form = new BootstrapFormBuilder(self::$formName);
        $this->form->setFormTitle("Grupo de notas da disciplina");
        $this->form->setFieldSizes('100%');

        $this->criteria = new TCriteria;
        $id             = new THidden('id');
        $grupos         = new THidden('grupos');
        $ref_disciplina = new TDBUniqueSearch('ref_disciplina', Conn::ALFA, 'Disciplina', 'id', 'descricao', 'descricao ASC', $this->criteria);
        
        $this->template = new THtmlRenderer('app/resources/html/academico/template-grupo-notas-disciplina.html');

        $grupos->setValue(0);

        $this->template->enableSection('main');
        $ref_disciplina->setMask("{id} - {descricao}");
        $ref_disciplina->setOperator('ilike');
        $ref_disciplina->setService('UnivatesMultiSearchService');
        $ref_disciplina->setMinLength(1);

        $row = $this->form->addFields([$id, $grupos]);
        $row->style = 'display: none';
        $row = $this->form->addFields([new TLabel("Disciplina"), $ref_disciplina]);
        $row->layout = ['col-sm-6'];
        $this->form->addFields([$this->template]);

        $btn_onsave = $this->form->addAction("Salvar", new TAction([$this, 'onSave']), 'fa:save #ffffff');
        $btn_onsave->addStyleClass('btn-primary');
        $this->form->addAction("Simular", new TAction(['SimulacaoGrupoNotaDisciplinaWindow', 'onLoad']), 'fa:eye orange');
        $this->form->addAction("Voltar para a listagem", new TAction(['GrupoNotaDisciplinaList', 'onLoad']), 'fa:table blue');

        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->class = 'form-container';
        $container->add(TBreadCrumb::create(["Acadêmico", "Web diário", "Grupo de notas da disciplina"]));
        $container->add($this->form);
        parent::add($container);
    }
    public static function onAddCriterios($param, $selecionados = NULL)
    {
        try
        {
            TTransaction::open(Conn::ALFA);
            $index = $param['grupo'];
            $ref_grupo = $param["ref_grupo"]??$param["ref_grupo_{$index}"]??NULL;
            if(empty($ref_grupo))
            {
                return;
            }
            foreach ($param as $key => $value)
            {
                if(strpos($key, 'ref_grupo_') === FALSE OR  $key === "ref_grupo_{$index}")
                {
                    continue;
                }
                if($value === $ref_grupo)
                {
                    new TToast(TToast::TYPE_ERROR, 'Você já selecionou esse grupo');
                    TScript::create("$('#criterios_{$index}').html('');$('[name=ref_grupo_{$index}]').val('').change();");
                    return;
                }
            }
            $grupo = GrupoNota::find($ref_grupo);
            $criterios = $grupo->getCriterioNotas(true);
            $div = new TElement('div');
            $div->class = 'col col-sm-12';
            $div->style = 'display: block; padding: 15px 0 0 0;';
            foreach($criterios as $criterio)
            {
                $check = new TEntry("criterio_{$index}[]");
                $check->setValue($criterio->id);
                $check->setName("criterio_{$index}[]");
                $check->setInputType('checkbox');
                $check->id = "criterio_{$index}_{$criterio->id}";
                $check->class ="filled-in chk-col-blue hidden";
                if(in_array($criterio->id, $selecionados))
                {
                    $check->checked = 'checked';
                }
                $label = new TElement('label');
                $label->{'for'} = "criterio_{$index}_{$criterio->id}";
                $label->add($criterio->nome);
                $label->style = 'width: 25%;float: left;';
                $div->add($check);
                $div->add($label);
            }
            TTransaction::close();
            TScript::create("$('#criterios_{$index}').html(`{$div->getContents()}`);");
            TScript::create("$('.grupo[id*=_grupo_{$index}] span').html(`{$grupo->nome}`);");
        }
        catch (Exception $e)
        {
            TTransaction::rollback();
            new TToast(TToast::TYPE_ERROR, 'Houve um erro ao buscar os critérios');
        }
    }
    public static function addNota($param, $grupoNotaDisciplina = NULL)
    {
        try
        {
            $result = [];
            for ($i=1; $i <= 3 ; $i++)
            {
                $cheked_recebimento = $grupoNotaDisciplina ? in_array($i, $grupoNotaDisciplina->notas_recebem_peso_grupos??[]) : FALSE;
                $cheked_desativacao = $grupoNotaDisciplina ? in_array($i, $grupoNotaDisciplina->notas_permitem_desativacao??[]) : FALSE;
                $replaces = [
                    "grupos" => $param['grupos'] + 1,
                    "nome" => $grupoNotaDisciplina ? $grupoNotaDisciplina->grupo_nota->nome : '',
                    "peso" => $grupoNotaDisciplina ? $grupoNotaDisciplina->peso : '-',
                    "cheked_recebimento" => $cheked_recebimento ? 'checked' : ($cheked_desativacao ? 'disabled' : ''),
                    "cheked_desativacao" => $cheked_desativacao ? 'checked' : ($cheked_recebimento ? 'disabled' : ''),
                    "nota" => $i,
                ];
                $template = new THtmlRenderer('app/resources/html/academico/grupo-notas-disciplina-detalhes-grupo.html');
                $template->enableSection('main', $replaces);
                $result[$i] = $template;
                if(empty($grupoNotaDisciplina))
                {
                    TScript::create("$('.gruponota.n{$i} .grupos').append(`{$template->getContents()}`);");
                }
            }
            return $result;
        }
        catch (Exception $e)
        {
            TTransaction::rollback();
            new TToast(TToast::TYPE_ERROR, 'Houve um erro ao adicionar a nota');
        }
    }
    public static function addGrupo($param, $grupoNotaDisciplina = NULL)
    {
        $grupos = $param['grupos'] + 1;
        $permite_desativacao = new TMultiSearch("permite_desativacao_{$grupos}");
        $recebem_pesos = new TMultiSearch("recebem_pesos_{$grupos}");
        $grupo = new TDBUniqueSearch("ref_grupo_{$grupos}", Conn::ALFA, 'GrupoNota', 'id', 'nome', 'nome ASC');
        $peso = new TEntry("peso_{$grupos}");
        $fl_nota_modular = new TRadioGroup("fl_nota_modular_{$grupos}");
        $grupo->{'onchange'} = "__adianti_post_data('form_AcdGrupoNotaDisciplina', 'class=GrupoNotaDisciplinaForm&method=onAddCriterios&static=1&grupo={$grupos}');";
        $fl_nota_modular->setLayout('horizontal');
        $fl_nota_modular->setUseButton();
        $fl_nota_modular->addItems([
            1 => 'Sim',
            0 => 'Não'
        ]);
        $permite_desativacao->setSize('100%', 35);
        $recebem_pesos->setSize('100%', 35);
        $grupo->setSize('100%');
        $peso->setSize('100%');
        $peso->setMask('999');
        $peso->{'style'} = 'text-align: right; padding-right: 5px;';
        $peso->{'onchange'} = 'changePeso(this)';
        $peso->{"data-grupo"} = $grupos;
        $permite_desativacao->setMinLength(0);
        $grupo->setMinLength(0);
        $recebem_pesos->setMinLength(0);
        if($grupoNotaDisciplina)
        {
            $permite_desativacao->setValue($grupoNotaDisciplina->notas_permitem_desativacao);
            $recebem_pesos->setValue($grupoNotaDisciplina->notas_recebem_peso_grupos);
            $grupo->setValue($grupoNotaDisciplina->ref_grupo_nota);
            $fl_nota_modular->setValue($grupoNotaDisciplina->fl_nota_modular ? '1' : '0');
            $peso->setValue($grupoNotaDisciplina->peso);
        }
        else
        {
            $fl_nota_modular->setValue('0');
        }
        $permite_desativacao->addItems(['1' => 'Nota 1', '2' => 'Nota 2', '3' => 'Nota 3']);
        $recebem_pesos->addItems(['1' => 'Nota 1', '2' => 'Nota 2', '3' => 'Nota 3']);
        $replaces = [
            'permite_desativacao' => $permite_desativacao,
            'recebem_pesos' => $recebem_pesos,
            'grupo' => $grupo,
            'fl_nota_modular' => $fl_nota_modular,
            'peso' => $peso,
            'key' => $grupos,
            'numero' => str_pad($grupos, 2, '0', STR_PAD_LEFT)
        ];
        $template = new THtmlRenderer('app/resources/html/academico/grupo-notas-disciplina.html');
        $template->enableSection('main', $replaces);
        if($grupoNotaDisciplina)
        {
            $ids = array_column($grupoNotaDisciplina->getCriterioNotaDisciplinas(), 'ref_criterio_nota');
            $criterios = self::onAddCriterios(
                ['grupo' => $grupos, 'ref_grupo' => $grupoNotaDisciplina->ref_grupo_nota],
                $ids
            );
            $template->enableSection('criterios', ['content' => $criterios]);
        }
        if(empty($grupoNotaDisciplina))
        {
            $content = str_replace('/', '\/', $template->getContents());
            TScript::create("$('#gruponotadisciplina').append(`{$content}`);");
            $obj = new stdClass;
            $obj->grupos = $grupos;
            TForm::sendData(self::$formName, $obj);
            self::addNota($param);
        }
        else
        {
            return $template;
        }
    }
    public static function onSave($param = null)
    {
        try
        {
            TTransaction::open(Conn::ALFA);
            if(empty($param['ref_disciplina']) AND empty($param['id']))
            {
                throw new Exception("Selecione a disciplina", -100);
            }
            if(empty($param['grupos']) OR $param['grupos'] < 1)
            {
                throw new Exception("Informe um grupo de notas", -100);
            }
            GrupoNotaDisciplina::deleteByDisciplina($param["ref_disciplina"]??$param['id']);
            $percentual   = 0;
            $desativacoes = [];
            $recebimentos = [];
            for ($i=1; $i <= $param['grupos'] ; $i++)
            {
                if(! isset($param["ref_grupo_{$i}"]))
                {
                    continue;
                }
                if(empty($param["ref_grupo_{$i}"]) OR empty($param["peso_{$i}"]))
                {
                    throw new Exception("Grupo e peso são campos obrigatórios", -100);
                }
                $grupoNotaDisciplina = new GrupoNotaDisciplina;
                $grupoNotaDisciplina->ref_disciplina             = $param["ref_disciplina"]??$param['id'];
                $grupoNotaDisciplina->ref_grupo_nota             = $param["ref_grupo_{$i}"];
                $grupoNotaDisciplina->peso                       = $param["peso_{$i}"];
                $grupoNotaDisciplina->notas_recebem_peso_grupos  = $param["recebem_pesos_{$i}"]??NULL;
                $grupoNotaDisciplina->notas_permitem_desativacao = $param["permite_desativacao_{$i}"]??NULL;
                $grupoNotaDisciplina->fl_nota_modular            = $param["fl_nota_modular_{$i}"]??NULL;
                if($grupoNotaDisciplina->notas_permitem_desativacao)
                {
                    foreach ($grupoNotaDisciplina->notas_permitem_desativacao as $nota)
                    {
                        $desativacoes[$grupoNotaDisciplina->ref_grupo_nota][] = $nota;
                    }
                }
                if($grupoNotaDisciplina->notas_recebem_peso_grupos)
                {
                    foreach ($grupoNotaDisciplina->notas_recebem_peso_grupos as $nota)
                    {
                        $recebimentos[$grupoNotaDisciplina->ref_grupo_nota][] = $nota;
                    }
                }
                $grupoNotaDisciplina->store();
                if(empty($param["criterio_{$i}"]))
                {
                    throw new Exception("Você não marcou os critérios do grupo {$grupoNotaDisciplina->grupo_nota->nome}", -100);
                }
                foreach ($param["criterio_{$i}"] as $refCriterio)
                {
                    $criterio = new CriterioNotaDisciplina;
                    $criterio->ref_grupo_nota_disciplina = $grupoNotaDisciplina->id;
                    $criterio->ref_criterio_nota = $refCriterio;
                    $criterio->store();
                }
                $percentual += $grupoNotaDisciplina->peso;
            }
            // validar dinamismo de notas
           self::validarDinamismo($desativacoes, $recebimentos);
            if($percentual == 0)
            {
                throw new Exception("Adicione grupos com pesos cujo soma seja um total de 100", -100);
            }
            if($percentual !== 100)
            {
                throw new Exception("Os pesos dos grupos devem somar um total de 100", -100);
            }
            TTransaction::close();
            new TToast(TToast::TYPE_SUCCESS, 'Salvo com sucesso');
        }
        catch (Exception $e)
        {
            SigmaException::showException($e);
            TTransaction::rollback();
        }
    }
    public static function validarDinamismo($desativacoes, $recebimentos)
    {
        if(empty($desativacoes))
        {
            return;
        }
        foreach ($desativacoes as $grupo => $notas)
        {
            foreach ($notas as $nota)
            {
                // Verifica se existe um grupo que não é desativado na mesma nota para receber o peso
                $recebe = 0;
                foreach ($recebimentos as $grupoRecebemPeso => $notasRecebemPeso)
                {
                    // Mesmo grupo não pode desativar e receber
                    if($grupoRecebemPeso == $grupo)
                    {
                        continue;
                    }
                    if(in_array($nota, $notasRecebemPeso))
                    {
                        $recebe++;
                    }
                }
                if($recebe < 1)
                {
                    throw new Exception("A nota {$nota} possui grupos que podem ser desativados e não possuem grupos para receber os pesos", -100);
                }
            }
        }
    }
    public function onEdit( $param )
    {
        try
        {
            TTransaction::open(Conn::ALFA);
            $gruposNotasDisciplina = GrupoNotaDisciplina::getByDisciplina($param['key']);
            $grupos = 0;
            if($gruposNotasDisciplina)
            {
                $contentsgrupos = [];
                $contentsnotas  = [];
                foreach ($gruposNotasDisciplina as $key => $grupoNotaDisciplina)
                {
                    $grupos++;
                    $contentsgrupos[] = [
                        'grupos' => self::addGrupo(['grupos' => $key], $grupoNotaDisciplina)
                    ];
                    $notas = self::addNota(['grupos' => $key], $grupoNotaDisciplina);
                    $nota1 = array_filter($notas, function($key) { return $key == 1; }, ARRAY_FILTER_USE_KEY);
                    $nota2 = array_filter($notas, function($key) { return $key == 2; }, ARRAY_FILTER_USE_KEY);
                    $nota3 = array_filter($notas, function($key) { return $key == 3; }, ARRAY_FILTER_USE_KEY);
                    $contentsnotas[1][] = ["grupo" => array_shift($nota1)];
                    $contentsnotas[2][] = ["grupo" => array_shift($nota2)];
                    $contentsnotas[3][] = ["grupo" => array_shift($nota3)];
                }
                $this->template->enableSection('nota1',  $contentsnotas[1], TRUE);
                $this->template->enableSection('nota2',  $contentsnotas[2], TRUE);
                $this->template->enableSection('nota3',  $contentsnotas[3], TRUE);
                $this->template->enableSection('grupos', $contentsgrupos,   TRUE);
            }
            $obj = new stdClass;
            $obj->ref_disciplina = $param['key'];
            $obj->grupos = $grupos;
            $obj->id = $param['key'];
            TForm::sendData(self::$formName, $obj);
            TMultiSearch::disableField(self::$formName, 'ref_disciplina');
            TTransaction::close();
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }
    public function onShow($param = null)
    {
        $this->criteria->add(new TFilter('NOT', 'EXISTS', 'NOESC: (SELECT 1 FROM acd_grupo_nota_disciplina WHERE ref_disciplina = acd_disciplinas.id)'));
        $this->template->enableSection('grupos', ['grupos' => self::addGrupo(['grupos' => 0])]);
        $obj = new stdClass;
        $obj->grupos = 1;
        TForm::sendData(self::$formName, $obj);
    }
}