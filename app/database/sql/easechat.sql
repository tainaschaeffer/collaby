CREATE TABLE activity( 
      id  SERIAL    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
      description text   NOT NULL  , 
      class_id integer   NOT NULL  , 
 PRIMARY KEY (id)) ; 

CREATE TABLE class( 
      id  SERIAL    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
      institution_id integer   NOT NULL  , 
 PRIMARY KEY (id)) ; 

CREATE TABLE groups( 
      id  SERIAL    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
      class_id integer   NOT NULL  , 
 PRIMARY KEY (id)) ; 

CREATE TABLE institution( 
      id  SERIAL    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
 PRIMARY KEY (id)) ; 

CREATE TABLE message( 
      id  SERIAL    NOT NULL  , 
      subject varchar  (100)   NOT NULL  , 
      message text   NOT NULL  , 
      student_group_id integer   NOT NULL  , 
      activity_id integer   NOT NULL  , 
      student_id integer NOT NULL,
      date_message TIMESTAMP NOT NULL,
 PRIMARY KEY (id)) ; 

CREATE TABLE student_class( 
      id  SERIAL    NOT NULL  , 
      class_id integer   NOT NULL  , 
      student_id integer   NOT NULL  , 
 PRIMARY KEY (id)) ; 

CREATE TABLE student_group(
 	id SERIAL NOT NULL,
 	group_id INT NOT NULL,
 	student_id INT NOT NULL,
 PRIMARY KEY(id)
 );

CREATE TABLE answer(
      id SERIAL NOT NULL,
      message_id INT NOT NULL,
      answer TEXT NOT NULL,
      student_id INT NOT NULL,
      date_answer TIMESTAMP NOT NULL,
 PRIMARY KEY(id)
);

ALTER TABLE activity ADD CONSTRAINT fk_activity_1 FOREIGN KEY (class_id) references class(id); 
ALTER TABLE class ADD CONSTRAINT fk_class_1 FOREIGN KEY (institution_id) references institution(id); 
ALTER TABLE group ADD CONSTRAINT fk_group_1 FOREIGN KEY (class_id) references class(id); 
ALTER TABLE message ADD CONSTRAINT fk_message_1 FOREIGN KEY (student_class_id) references student_class(id); 
ALTER TABLE message ADD CONSTRAINT fk_message_2 FOREIGN KEY (activity_id) references activity(id); 
ALTER TABLE student_class ADD CONSTRAINT fk_student_class_1 FOREIGN KEY (class_id) references class(id); 
ALTER TABLE message ADD CONSTRAINT fk_message_1 FOREIGN KEY (student_group_id) references student_group(id); 
ALTER TABLE answer ADD CONSTRAINT fk_answer_1 FOREIGN KEY (message_id) references message(id); 

SELECT setval('activity_id_seq', coalesce(max(id),0) + 1, false) FROM activity;
SELECT setval('class_id_seq', coalesce(max(id),0) + 1, false) FROM class;
SELECT setval('group_id_seq', coalesce(max(id),0) + 1, false) FROM group;
SELECT setval('institution_id_seq', coalesce(max(id),0) + 1, false) FROM institution;
SELECT setval('message_id_seq', coalesce(max(id),0) + 1, false) FROM message;
SELECT setval('student_class_id_seq', coalesce(max(id),0) + 1, false) FROM student_class;
SELECT setval('student_group_id_seq', coalesce(max(id),0) + 1, false) FROM student_group;
SELECT setval('answer_id_seq', coalesce(max(id),0) + 1, false) FROM answer;