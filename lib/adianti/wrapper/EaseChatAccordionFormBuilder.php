<?php
/**
 * @author Tainá Schaeffer
 */
class EaseChatAccordionFormBuilder extends AccordionFormBuilder
{
	private static $database = 'tcc';
	public function __construct($formName = NULL, $title = NULL, $visibility_control = TRUE)
	{
		parent::__construct($formName, $visibility_control);
		$this->setFieldSizes('100%');
   	$this->setFormTitle($title);
   	$this->generateAria();
    
    $visible = parent::isVisible($formName);
    
    if($visible === NULL)
    {
      TTransaction::open('permission');
      $login   = TSession::getValue('login');
      $user_id = SystemUsers::idFromLogin($login);
      TTransaction::close();

      TTransaction::open(self::$database);
      
      $preference = Preference::getPreferenceByUser($user_id);
      if($preference)
      {
        $visible = $preference->fl_filter_hidden ? FALSE : TRUE;
      }

      TTransaction::close();
    }
    $this->setVisible($visible);
	}
}