CREATE TABLE activity( 
      id  INT IDENTITY    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
      description nvarchar(max)   NOT NULL  , 
      class_id int   NOT NULL  , 
 PRIMARY KEY (id)) ; 

CREATE TABLE class( 
      id  INT IDENTITY    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
      institution_id int   NOT NULL  , 
 PRIMARY KEY (id)) ; 

CREATE TABLE group( 
      id  INT IDENTITY    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
      class_id int   NOT NULL  , 
 PRIMARY KEY (id)) ; 

CREATE TABLE institution( 
      id  INT IDENTITY    NOT NULL  , 
      name varchar  (45)   NOT NULL  , 
 PRIMARY KEY (id)) ; 

CREATE TABLE message( 
      id  INT IDENTITY    NOT NULL  , 
      subject varchar  (100)   NOT NULL  , 
      message nvarchar(max)   NOT NULL  , 
      student_class_id int   NOT NULL  , 
      activity_id int   NOT NULL  , 
 PRIMARY KEY (id)) ; 

CREATE TABLE student_class( 
      id  INT IDENTITY    NOT NULL  , 
      class_id int   NOT NULL  , 
      student_id int   NOT NULL  , 
 PRIMARY KEY (id)) ; 

 
  
 ALTER TABLE activity ADD CONSTRAINT fk_activity_1 FOREIGN KEY (class_id) references class(id); 
ALTER TABLE class ADD CONSTRAINT fk_class_1 FOREIGN KEY (institution_id) references institution(id); 
ALTER TABLE group ADD CONSTRAINT fk_group_1 FOREIGN KEY (class_id) references class(id); 
ALTER TABLE message ADD CONSTRAINT fk_message_1 FOREIGN KEY (student_class_id) references student_class(id); 
ALTER TABLE message ADD CONSTRAINT fk_message_2 FOREIGN KEY (activity_id) references activity(id); 
ALTER TABLE student_class ADD CONSTRAINT fk_student_class_1 FOREIGN KEY (class_id) references class(id); 

  
