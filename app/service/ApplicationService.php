<?php

/**
 * ApplicationService
 * Date: 21-09-2020
 *
 * @version    2.0
 * @author     Tainá Schaeffer
 * @copyright  Copyright (c) 2006-2014 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */

class ApplicationService
{
    public static function getLogin()
    {
        $login = TSession::getValue('login_personified');
        if(!$login)
        {
            $login = TSession::getValue('login');
        }

        return $login;
    }

    public static function getUserID()
    {
        $userid = TSession::getValue('userid_personified');
        if(!$userid)
        {
            $userid = TSession::getValue('userid');
        }

        return $userid;
    }

    public static function getUserGroupsIDS()
    {
        $usergroupids = TSession::getValue('usergroupids_personified');
        if(!$usergroupids)
        {
            $usergroupids = TSession::getValue('usergroupids');
        }

        return $usergroupids;
    }

    public static function getUserUnitIDS()
    {
        $userunitids = TSession::getValue('userunitids_personified');
        if(!$userunitids)
        {
            $userunitids = TSession::getValue('userunitids');
        }

        return $userunitids;
    }

    public static function getUserName()
    {
        $username = TSession::getValue('username_personified');
        if(!$username)
        {
            $username = TSession::getValue('username');
        }

        return $username;
    }

    public static function getUserMail()
    {
        $usermail = TSession::getValue('usermail_personified');
        if(!$usermail)
        {
            $usermail = TSession::getValue('usermail');
        }

        return $usermail;
    }

    public static function getFrontPage()
    {
        $frontpage = TSession::getValue('frontpage_personified');
        if(!$frontpage)
        {
            $frontpage = TSession::getValue('frontpage');
        }

        return $frontpage;
    }

    public static function getPrograms()
    {
        $programs = TSession::getValue('programs_personified');
        if(!$programs)
        {
            $programs = TSession::getValue('programs');
        }

        return $programs;
    }
}