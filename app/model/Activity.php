<?php

class Activity extends TRecord
{
    const TABLENAME  = 'activity';
    const PRIMARYKEY = 'id';
    const IDPOLICY   =  'serial'; // {max, serial}

    private $class;

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('name');
        parent::addAttribute('description');
        parent::addAttribute('class_id');
        parent::addAttribute('active');
    }

    /**
     * Method set_class
     * Sample of usage: $var->class = $object;
     * @param $object Instance of Class
     */
    public function set_class(StudyClass $object)
    {
        $this->class = $object;
        $this->class_id = $object->id;
    }

    /**
     * Method get_class
     * Sample of usage: $var->class->attribute;
     * @returns Class instance
     */
    public function get_class()
    {
    
        // loads the associated object
        if (empty($this->class))
            $this->class = new StudyClass($this->class_id);
    
        // returns the associated object
        return $this->class;
    }

    public function get_active_description()
    {
        return $this->active ? 'Sim' : 'Não';
    }

    /**
     * Method getMessages
     */
    public function getMessages()
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('activity_id', '=', $this->id));
        return Message::getObjects( $criteria );
    }

    public static function getLastActivity($class_id)
    {
        return Activity::where('class_id', '=', "$class_id")
                       ->orderBy('id', 'desc')
                       ->take(1)
                       ->load();
    }

    
}

