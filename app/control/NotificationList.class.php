<?php
/**
 * NotificationList
 *
 * @version    1.0
 * @package    control
 * @author     Pablo Dall'Oglio
 * @copyright  Copyright (c) 2006 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */
class NotificationList extends TElement
{
    public function __construct()
    {
        try
        {
            TTransaction::open('collaby');

            parent::__construct('ul');
            $this->class     = 'dropdown-menu';
            $answers_message = [];
            $new_messages    = [];

            $messages = Message::getMessageBySdudentGroup(ApplicationService::getUserID());
            $answers  = Message::getAnswersMessage(ApplicationService::getUserID());

            if($answers)
            {
                foreach($answers as $answer)
                {
                    $answers_message[] = $answer;
                }
            }

            if($messages)
            {
                foreach ($messages as $message) 
                {
                    $new_messages[] = $message;
                }
            }

            $total = count($answers_message) + count($new_messages);

            $class_add = $total > 0 ? '' : '';

            $alink                  = new TElement('a');
            $ul_wrapper             = new TElement('ul');
            $li_master              = new TElement('li');
            $alink->{'class'}       = "dropdown-toggle";
            $alink->{'data-toggle'} = "dropdown";
            $alink->{'href'}        = "#";
            $alink->add(TElement::tag('i',    '', array('class' => "fas fa-bell")));
            $alink->add(TElement::tag('span', $total, array('class'=>"label-count {$class_add}")));
            $alink->show();
            $ul_wrapper->{'class'} = 'menu';
            $ul_wrapper->{'style'} = 'list-style:none';
            $li_master->{'class'}  = 'body';
            $li_master->add($ul_wrapper);

            if (empty($answers_message) && empty($new_messages))
            {
                $li            = new TElement('li');
                $li->{'class'} = 'no-context';
                $li->add('Sem notificações');
                $ul_wrapper->add($li);
            }
            
            $count = 0;
            if($answers_message)
            {
                foreach ($answers_message as $answer)
                {
                    $count++;
                    if($count >= 10)
                    {
                        break;
                    }
                    $ul_wrapper->add(self::makeLineAnswer($answer));
                }
            }


            if($new_messages)
            {
                foreach ($new_messages as $message) 
                {
                    $count++;
                    if($count >= 10)
                    {
                        break;
                    }
                    $ul_wrapper->add(self::makeLineMessage($message));
                }
            }

            parent::add(TElement::tag('li', 'Notificações', ['class'=>'header']));
            parent::add($li_master);
            parent::add(TElement::tag('li', TElement::tag('a', 'Ver todas', ['href'=>'index.php?class=DiscussionList&method=onShow', 'generator'=>'adianti']), ['class'=>'footer']));

            TTransaction::close();
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
        }
    }

    private static function makeLineMessage($message)
    {
        $action = new TAction(['DiscussionList', 'onShow']);

        $replaces = [
            'nome_pessoa' => 'Mensagem de '.Message::getStudentName($message->id),
            'action' => $action->serialize(),
            'nome_atividade' => $message->activity->name,
            'mensagem' => $message->message
        ];

        $template = new THtmlRenderer('app/resources/html/alerta.html');
        $template->enableSection('main', $replaces);

        return $template;
    }

    private static function makeLineAnswer($answer)
    {
        $answer  = new Answer($answer->id);
        $message = new Message($answer->message_id);

        $action = new TAction(['DiscussionForm', 'onParticipate'], ['key' => $message->id]);

        $replaces = [
            'nome_pessoa' => 'Resposta de '.$answer->student_name,
            'action' => $action->serialize(),
            'nome_atividade' => $message->activity->name,
            'mensagem' => $answer->answer
        ];

        $template = new THtmlRenderer('app/resources/html/alerta.html');
        $template->enableSection('main', $replaces);

        return $template;
    }
}
