<?php

class DashboardView extends TPage
{
    private $container;
    private $fieldlist;
    private $filter_criteria;
    private $filter_criteria2;
    private static $formName = 'form_Dashboard';

    private $form;      // search form
    private $datagrid;  // listing
    private $datagrid2;  // listing
    private $pageNavigation;
    
    use Adianti\Base\AdiantiStandardListTrait;

    public function __construct($param)
    {
        parent::__construct();

        $this->setDatabase('collaby'); // defines the database
        $this->setActiveRecord('Message'); // defines the active record
        $this->setDefaultOrder('id', 'asc');  // defines the default order
        $this->addFilterField('id', '=', 'id'); // add a filter field
        
        TTransaction::open('collaby');

        $this->container = new TVBox;
        $this->container->style = 'width: 100%';
        $this->container->add(TBreadCrumb::create(['Colaboração', 'Alunos colaborativos solícitos']));

        $this->form = new BootstrapFormBuilder(self::$formName, 'Filtros');

        $btn_onsearch = $this->form->addAction('Buscar', new TAction([__CLASS__, 'onFilter'], $param), 'fa:search #ffffff');
        $btn_onsearch->addStyleClass('btn-primary');

        $this->makeWindowGeneral();

        $this->container->add($this->form);

        parent::add($this->container);
        TTransaction::close();

        /*TScript::create("$('#leftsidebar').addClass('menu-hide');
                         $('.action-hide').addClass('close-menu');
                         $('.content').addClass('content-hide');");*/
    }

    private function makeWindowGeneral()
    {
        $this->form->appendPage('Geral');

        $criteria_class = new TCriteria;
        $criteria_group = new TCriteria;

        $user_id = ApplicationService::getUserID();
        $groups  = ApplicationService::getUserGroupsIDS();
        if(in_array(3, $groups))
        {
            $criteria_class->add(new TFilter('teacher_id', '=', $user_id));
            //$criteria_group->add(new TFilter('class_id', 'IN', "(SELECT id FROM class WHERE teacher_id = {$user_id})"));
        }

        $class_id     = new TDBCombo('class_id', 'collaby', 'StudyClass', 'id', '{name}','id asc', $criteria_class);
        //$group_id     = new TDBCombo('group_id', 'collaby', 'Group', 'id', '{name}','id asc', $criteria_group);
        $date_process = new TDBCombo('date_process', 'collaby', 'ProcessData', 'date_process', 'date_process','date_process asc');

        $class_id->setChangeAction(new TAction([$this, 'onChangeClass']));
        //$group_id->setChangeAction(new TAction([$this, 'onChangeGroup']));

        $class_id->setSize('100%');
        //$group_id->setSize('100%');
        $date_process->setSize('100%');

        $row         = $this->form->addFields([new TLabel('Turma*'), $class_id], [new TLabel('Data processamento*'), $date_process]);
        $row->layout = ['col-sm-8', 'col-sm-4'];

        //$row         = $this->form->addFields([new TLabel('Aluno'), $student_id]);
        //$row->layout = ['col-sm-12'];
    }

    /*public static function onChangeClass($param)
    {
        try
        {
            if($param['class_id'])
            {
                TTransaction::open('collaby');

                $groups = Group::where('class_id', '=', $param['class_id'])->load();
                
                $array_groups = [];

                if ($groups)
                {
                    foreach ($groups as $group)
                    {
                        $array_groups[$group->id] = $group->name;
                    }
                }

                TDBCombo::reload(self::$formName, 'group_id', $array_groups, TRUE);
                
                TTransaction::close();
            }
        }
        catch(Execption $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }*/

    public static function onChangeClass($param)
    {
        try
        {
            if(isset($param['class_id']))
            {
                TTransaction::open('collaby');

                $criteria = new TCriteria;
                $criteria->add(new TFilter('class_id', '=', $param['class_id']));
                $criteria->setProperty('group', 'id, date_process');
                $criteria->setProperty('order', 'date_process desc');

                $process_data = ProcessData::getObjects($criteria);
                
                $array_data = [];

                if ($process_data)
                {
                    foreach ($process_data as $data)
                    {
                        $array_data[$data->date_process] = $data->date_process;
                    }
                }

                TDBCombo::reload(self::$formName, 'date_process', $array_data);
                
                TTransaction::close();
            }
        }
        catch(Execption $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public function onFilter($param)
    {
        try
        {
            TTransaction::open('collaby');

            $data = $this->form->getData();

            if(!$data->class_id)
            {
                new TMessage('error', "O campo Turma é obrigatório");
                return;
            }

            $this->makeDashboard($data);

            $this->form->setData($data);
            self::onChangeClass(['class_id' => $data->class_id]);

            TTransaction::close();
        }
        catch(Execption $e)
        {
            $this->form->setData($data);
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public function makeDashboard($data)
    {
        $html = new THtmlRenderer('app/resources/dashboard.html');
            
        TTransaction::open('collaby');
        $indicator1 = new THtmlRenderer('app/resources/info-box.html');
        $indicator2 = new THtmlRenderer('app/resources/info-box.html');
        $indicator3 = new THtmlRenderer('app/resources/info-box.html');
        $indicator4 = new THtmlRenderer('app/resources/info-box.html');

        $criteria_group    = new TCriteria;
        $criteria_users    = new TCriteria;
        $criteria_messages = new TCriteria;
        $criteria_messages_discard = new TCriteria;
        $criteria_answer   = new TCriteria;
        $criteria_answer_discard = new TCriteria;

        if($data->class_id)
        {
            $criteria_group->add(new TFilter('class_id', '=', $data->class_id));
            $criteria_users->add(new TFilter('class_id', '=', $data->class_id));
            $criteria_messages->add(new TFilter('group_id', 'IN', "(SELECT groups.id FROM groups WHERE groups.class_id = {$data->class_id})"));
            $criteria_messages_discard->add(new TFilter('group_id', 'IN', "(SELECT groups.id FROM groups WHERE groups.class_id = {$data->class_id})"));
            $criteria_messages_discard->add(new TFilter('rating', 'IN', "NOESC:('irrelevante')"));
            $criteria_answer->add(new TFilter('message_id', 'IN', "(SELECT message.id FROM message, groups WHERE message.group_id = groups.id AND groups.class_id = {$data->class_id})"));
            $criteria_answer_discard->add(new TFilter('message_id', 'IN', "(SELECT message.id FROM message, groups WHERE message.group_id = groups.id AND groups.class_id = {$data->class_id})"));
            $criteria_answer_discard->add(new TFilter('rating', 'IN', "NOESC:('irrelevante')"));
        }   

        $total_students = count(StudentClass::getObjects($criteria_users));
        $total_messages = count(Message::getObjects($criteria_messages));
        $total_messages_discard = count(Message::getObjects($criteria_messages_discard));
        $total_answer   = count(Answer::getObjects($criteria_answer));
        $total_answer_discard   = count(Answer::getObjects($criteria_answer_discard));

        $total_messages -= $total_messages_discard;
        $total_answer -= $total_answer_discard;
        
        $indicator1->enableSection('main', ['title' => 'Total de mensagens', 'icon' => 'users', 'background' => 'blue', 'value' => ($total_messages+$total_answer)]);
        $indicator2->enableSection('main', ['title' => 'Total de perguntas', 'icon' => 'university', 'background' => 'purple', 'value' => $total_messages]);
        $indicator3->enableSection('main', ['title' => 'Total de respostas', 'icon' => 'code', 'background' => 'green',  'value' => $total_answer]);
        $indicator4->enableSection('main', ['title' => 'Alunos', 'icon' => 'user', 'background' => 'orange', 'value' => $total_students]);

        $html->enableSection('main', ['indicator1' => $indicator1,
                                      'indicator2' => $indicator2,
                                      'indicator3' => $indicator3,
                                      'indicator4' => $indicator4] );

        $this->datagrid2 = new TDataGrid;
        $this->datagrid2->disableHtmlConversion();
        $this->datagrid2 = new BootstrapDatagridWrapper($this->datagrid2);
        $this->filter_criteria2 = new TCriteria;

        $this->datagrid2->style = 'width: 100%';
        $this->datagrid2->setHeight(320);

        $column_student_group  = new TDataGridColumn('grupo', "Grupo", 'left', '10%');
        $column_student_name   = new TDataGridColumn('aluno', "Aluno", 'left', '30%');
        $column_observavel_s   = new TDataGridColumn('observavel_s', "Observável S", 'center', '15%');
        $column_observavel_p   = new TDataGridColumn('observavel_p', "Observável P", 'center', '15%');
        $column_observavel_r   = new TDataGridColumn('observavel_r', "Observável R", 'center', '15%');
        $column_solicito_ativo = new TDataGridColumn('solicito_ativo', "Solícito ativo", 'center', '15%');
        $this->datagrid2->addColumn($column_student_group);
        $this->datagrid2->addColumn($column_student_name);
        $this->datagrid2->addColumn($column_observavel_s);
        $this->datagrid2->addColumn($column_observavel_p);
        $this->datagrid2->addColumn($column_observavel_r);
        $this->datagrid2->addColumn($column_solicito_ativo);
        
        // creates the datagrid model
        $this->datagrid2->createModel();

        $results = ProcessData::getResultsByClassAndDateProcess($data->class_id, $data->date_process);

        $this->datagrid2->clear();
        if($results)
        {
            // iterate the collection of active records
            foreach ($results as $result)
            {
                // add the object inside the datagrid

                $this->datagrid2->addItem($result);
            }
        }

        TSession::setValue('dados_analise_indicadores_solicitude', $results);

        $panel2 = new TPanelGroup('Análise de solicitude');
        $panel2->add($this->datagrid2)->style = 'overflow-x:auto';
        //$panel2->addFooter('Total de discussões: '.$total_messages);
        
        $dropdown2 = new TDropDown(_t('Export'), 'fa:list');
        $dropdown2->setPullSide('right');
        $dropdown2->setButtonClass('btn btn-default waves-effect dropdown-toggle');
        $dropdown2->addAction( _t('Save as CSV'), new TAction([$this, 'onExportCsv'], ['class_id'     => $data->class_id,
                                                                                       'date_process' => $data->date_process]), 'fa:table fa-fw blue' );
        $panel2->addHeaderWidget( $dropdown2 );

        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add($html);
        $container->add($panel2);
        //$container->add($panel);
        
        parent::add($container);

        TTransaction::close();
    }

    public function onExportCsv($param = null) 
    {
        try
        {
            $data               = new stdClass;
            $data->class_id     = $param['class_id'];
            $data->date_process = $param['date_process'];

            $records = TSession::getValue('dados_analise_indicadores_solicitude');

            if ($records)
            {
                $file = 'tmp/'.uniqid().'.csv';
                $handle = fopen($file, 'w');

                $csvColumns   = [];
                $csvColumns[] = 'Grupo';
                $csvColumns[] = 'Aluno';
                $csvColumns[] = 'Papel no grupo';
                $csvColumns[] = 'Nota inicial';
                $csvColumns[] = 'Nota final';
                $csvColumns[] = 'Intervenções';
                $csvColumns[] = 'Intervenções relevantes';
                $csvColumns[] = 'Intervenções solícitas';
                $csvColumns[] = 'Tamanho médio mensagens do aluno';
                $csvColumns[] = 'Tamanho médio mensagens dos alunos não solícitos';
                $csvColumns[] = 'Tamanho médio mensagens relevantes';
                $csvColumns[] = 'Quantidade de perguntas do grupo';
                $csvColumns[] = 'Quantidade de perguntas respondidas';
                $csvColumns[] = 'Índice de participação';
                $csvColumns[] = '% de mensagens classificadas solícitas pelo minerador';
                $csvColumns[] = '% de mensagens consideradas relevantes';
                $csvColumns[] = 'Observável S';
                $csvColumns[] = 'Observável P';
                $csvColumns[] = 'Observável R';
                $csvColumns[] = 'Solícito ativo';

                fputcsv($handle, $csvColumns, ';');
                foreach ($records as $record)
                {
                    $csvColumns = [];
                    foreach($record as $column)
                    {
                        $csvColumns[] = $column;
                    }
                    fputcsv($handle, $csvColumns, ';');
                }

                fclose($handle);

                TPage::openFile($file);
                
                $this->makeDashboard($data);

                $this->form->setData($data);
            }
            else
            {
                new TMessage('info', _t('No records found'));       
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
        }
    }

    public function makeReportMessages($criteria_messages)
    {
        $repository = new TRepository('Message');
        $limit = 20;

        $criteria = clone $criteria_messages;

        if (empty($param['order']))
        {
            $param['order'] = 'id';    
        }

        if (empty($param['direction']))
        {
            $param['direction'] = 'desc';
        }

        $criteria->setProperties($param); // order, offset
        $criteria->setProperty('limit', $limit);

        if($filters = TSession::getValue(__CLASS__.'_filters'))
        {
            foreach ($filters as $filter) 
            {
                $criteria->add($filter);       
            }
        }

        // load the objects according to criteria
        $objects = $repository->load($criteria, FALSE);

        $this->datagrid->clear();
        if ($objects)
        {
            // iterate the collection of active records
            foreach ($objects as $object)
            {
                // add the object inside the datagrid

                $this->datagrid->addItem($object);
            }
        }

        // reset the criteria for record count
        $criteria->resetProperties();
        $count= $repository->count($criteria);

        return $count;
    }

    public function makeReportProcessData($criteria_messages)
    {
        $repository = new TRepository('ProcessData');
        $limit = 20;

        $criteria = clone $criteria_messages;

        if (empty($param['order']))
        {
            $param['order'] = 'id';    
        }

        if (empty($param['direction']))
        {
            $param['direction'] = 'desc';
        }

        $criteria->setProperties($param); // order, offset
        $criteria->setProperty('limit', $limit);

        if($filters = TSession::getValue(__CLASS__.'_filters'))
        {
            foreach ($filters as $filter) 
            {
                $criteria->add($filter);       
            }
        }

        // load the objects according to criteria
        $objects = $repository->load($criteria, FALSE);

        $this->datagrid2->clear();
        if ($objects)
        {
            // iterate the collection of active records
            foreach ($objects as $object)
            {
                // add the object inside the datagrid

                $this->datagrid2->addItem($object);
            }
        }

        // reset the criteria for record count
        $criteria->resetProperties();
        $count= $repository->count($criteria);

        return $count;
    }
}