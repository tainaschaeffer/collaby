CREATE TABLE activity( 
      id number(10)    NOT NULL , 
      name varchar  (45)    NOT NULL , 
      description CLOB    NOT NULL , 
      class_id number(10)    NOT NULL , 
 PRIMARY KEY (id)) ; 

CREATE TABLE class( 
      id number(10)    NOT NULL , 
      name varchar  (45)    NOT NULL , 
      institution_id number(10)    NOT NULL , 
 PRIMARY KEY (id)) ; 

CREATE TABLE group( 
      id number(10)    NOT NULL , 
      name varchar  (45)    NOT NULL , 
      class_id number(10)    NOT NULL , 
 PRIMARY KEY (id)) ; 

CREATE TABLE institution( 
      id number(10)    NOT NULL , 
      name varchar  (45)    NOT NULL , 
 PRIMARY KEY (id)) ; 

CREATE TABLE message( 
      id number(10)    NOT NULL , 
      subject varchar  (100)    NOT NULL , 
      message CLOB    NOT NULL , 
      student_class_id number(10)    NOT NULL , 
      activity_id number(10)    NOT NULL , 
 PRIMARY KEY (id)) ; 

CREATE TABLE student_class( 
      id number(10)    NOT NULL , 
      class_id number(10)    NOT NULL , 
      student_id number(10)    NOT NULL , 
 PRIMARY KEY (id)) ; 

 
  
 ALTER TABLE activity ADD CONSTRAINT fk_activity_1 FOREIGN KEY (class_id) references class(id); 
ALTER TABLE class ADD CONSTRAINT fk_class_1 FOREIGN KEY (institution_id) references institution(id); 
ALTER TABLE group ADD CONSTRAINT fk_group_1 FOREIGN KEY (class_id) references class(id); 
ALTER TABLE message ADD CONSTRAINT fk_message_1 FOREIGN KEY (student_class_id) references student_class(id); 
ALTER TABLE message ADD CONSTRAINT fk_message_2 FOREIGN KEY (activity_id) references activity(id); 
ALTER TABLE student_class ADD CONSTRAINT fk_student_class_1 FOREIGN KEY (class_id) references class(id); 
 CREATE SEQUENCE activity_id_seq START WITH 1 INCREMENT BY 1; 

CREATE OR REPLACE TRIGGER activity_id_seq_tr 

BEFORE INSERT ON activity FOR EACH ROW 

WHEN 

(NEW.id IS NULL) 

BEGIN 

SELECT activity_id_seq.NEXTVAL INTO :NEW.id FROM DUAL; 

END;
CREATE SEQUENCE class_id_seq START WITH 1 INCREMENT BY 1; 

CREATE OR REPLACE TRIGGER class_id_seq_tr 

BEFORE INSERT ON class FOR EACH ROW 

WHEN 

(NEW.id IS NULL) 

BEGIN 

SELECT class_id_seq.NEXTVAL INTO :NEW.id FROM DUAL; 

END;
CREATE SEQUENCE group_id_seq START WITH 1 INCREMENT BY 1; 

CREATE OR REPLACE TRIGGER group_id_seq_tr 

BEFORE INSERT ON group FOR EACH ROW 

WHEN 

(NEW.id IS NULL) 

BEGIN 

SELECT group_id_seq.NEXTVAL INTO :NEW.id FROM DUAL; 

END;
CREATE SEQUENCE institution_id_seq START WITH 1 INCREMENT BY 1; 

CREATE OR REPLACE TRIGGER institution_id_seq_tr 

BEFORE INSERT ON institution FOR EACH ROW 

WHEN 

(NEW.id IS NULL) 

BEGIN 

SELECT institution_id_seq.NEXTVAL INTO :NEW.id FROM DUAL; 

END;
CREATE SEQUENCE message_id_seq START WITH 1 INCREMENT BY 1; 

CREATE OR REPLACE TRIGGER message_id_seq_tr 

BEFORE INSERT ON message FOR EACH ROW 

WHEN 

(NEW.id IS NULL) 

BEGIN 

SELECT message_id_seq.NEXTVAL INTO :NEW.id FROM DUAL; 

END;
CREATE SEQUENCE student_class_id_seq START WITH 1 INCREMENT BY 1; 

CREATE OR REPLACE TRIGGER student_class_id_seq_tr 

BEFORE INSERT ON student_class FOR EACH ROW 

WHEN 

(NEW.id IS NULL) 

BEGIN 

SELECT student_class_id_seq.NEXTVAL INTO :NEW.id FROM DUAL; 

END;
 
  
