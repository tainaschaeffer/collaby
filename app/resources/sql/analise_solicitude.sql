SELECT DISTINCT system_users.name AS aluno,
	   count(process_data.id) AS total_participacoes,
	   count(process_data_sol.id) AS total_participacoes_solicitas,
	   round((avg(process_data_sol.prob_1)*100)::numeric, 2) AS indice_solicitude,
	   count(process_data_answer.type) AS total_respostas,
	   count(process_data_answer_sol.type) AS total_respostas_solicitas,
	   round((avg(process_data_answer_sol.prob_1)*100)::numeric, 2) AS indice_solicitude_respostas
  FROM system_users INNER JOIN 
  	   process_data ON (process_data.student_id = system_users.id) LEFT JOIN
  	   (SELECT id, prob_0::float, prob_1::float, type FROM process_data WHERE class_id = :class_id AND date_process = :date_process AND prob_1::float > 0.50) process_data_sol ON (process_data.id = process_data_sol.id) LEFT JOIN
  	   (SELECT id, prob_0::float, prob_1::float, type FROM process_data WHERE class_id = :class_id AND date_process = :date_process AND type = 'R') process_data_answer ON (process_data.id = process_data_answer.id) LEFT JOIN
  	   (SELECT id, prob_0::float, prob_1::float, type FROM process_data WHERE class_id = :class_id AND date_process = :date_process AND type = 'R' AND prob_1::float > 0.50) process_data_answer_sol ON (process_data.id = process_data_answer_sol.id)
 WHERE process_data.class_id = :class_id
   AND process_data.date_process = :date_process
GROUP BY 1