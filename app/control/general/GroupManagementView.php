<?php

use Adianti\Database\TTransaction;

/**
 * GroupManagementView
 *
 * @version    1.0
 * @package    samples
 * @subpackage tutor
 * @author     Artur Comunello
 * @author     Pablo Dall'Oglio
 * @copyright  Copyright (c) 2006 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 */


class GroupManagementView extends TPage
{
    private static $formName = 'form_GroupManagement';

    private $container;
    private $form;
    private $kanbanGroups;
    
    public function __construct($param)
    {
        parent::__construct();
        
        $this->container = new TVBox;
        $this->container->style = 'width: 100%';
        $this->container->add(TBreadCrumb::create(['Colaboração', 'Gestão de grupos']));

        $this->form = new BootstrapFormBuilder(self::$formName, 'Filtros');

        $btn_onsearch = $this->form->addAction('Buscar', new TAction([__CLASS__, 'onFilter'], $param), 'fa:search #ffffff');
        $btn_onsearch->addStyleClass('btn-primary');

        $this->makeWindowFilters();
        
        $this->container->add($this->form);

        parent::add($this->container);
    }

    private function makeWindowFilters()
    {
        $this->form->appendPage('Geral');

        $criteria_class = new TCriteria;
        $criteria_group = new TCriteria;

        $user_id = ApplicationService::getUserID();
        $groups  = ApplicationService::getUserGroupsIDS();
        if(in_array(3, $groups))
        {
            $criteria_class->add(new TFilter('teacher_id', '=', $user_id));
        }

        $class_id     = new TDBCombo('class_id', 'collaby', 'StudyClass', 'id', '{name}','id asc', $criteria_class);
        $date_process = new TDBCombo('date_process', 'collaby', 'ProcessData', 'date_process', 'date_process','date_process asc');

        $class_id->setChangeAction(new TAction([$this, 'onChangeClass']));

        $class_id->setSize('100%');
        $date_process->setSize('100%');

        $row         = $this->form->addFields([new TLabel('Turma*'), $class_id], [new TLabel('Data processamento*'), $date_process]);
        $row->layout = ['col-sm-8', 'col-sm-4'];
    }

    public static function onChangeClass($param)
    {
        try
        {
            if(isset($param['class_id']))
            {
                TTransaction::open('collaby');

                $criteria = new TCriteria;
                $criteria->add(new TFilter('class_id', '=', $param['class_id']));
                $criteria->setProperty('group', 'id, date_process');
                $criteria->setProperty('order', 'date_process desc');

                $process_data = ProcessData::getObjects($criteria);

                $array_data = [];

                if ($process_data)
                {
                    foreach ($process_data as $data)
                    {
                        $array_data[$data->date_process] = $data->date_process;
                    }
                }

                TDBCombo::reload(self::$formName, 'date_process', $array_data);

                TTransaction::close();
            }
        }
        catch(Execption $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public function onFilter($param)
    {
        try
        {
            TTransaction::open('collaby');

            $data = $this->form->getData();

            if(!$data->class_id)
            {
                new TMessage('error', "O campo Turma é obrigatório");
                return;
            }

            if(!$data->date_process)
            {
                new TMessage('error', "O campo Data processamento é obrigatório");
                return;
            }

            $this->makeKanbanGroups($data);

            $this->form->setData($data);
            self::onChangeClass(['class_id' => $data->class_id]);

            TTransaction::close();
        }
        catch(Execption $e)
        {
            $this->form->setData($data);
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public function makeKanbanGroups($data)
    {
        try
        {
            if(isset($data->class_id))
            {
                TTransaction::open('collaby');

                $this->kanbanGroups = new TKanban;
                
                $groups = Group::where('class_id', '=', $data->class_id)->load();

                if(!$groups)
                {
                    new TMessage('error', "A turma selecionada não possui grupos!");
                    return;
                }

                $stages = [];
                foreach($groups as $group)
                {
                    $stages[] = ['id' => $group->id, 'title' => $group->name];
                }

                foreach ($stages as $stage)
                {
                    $this->kanbanGroups->addStage($stage['id'], $stage['title']);
                    $students = StudentGroup::getStudentsByGroup($stage['id']);
                    
                    if(!$students)
                    {
                        new TMessage('error', "Erro ao buscar os alunos!");
                        return;
                    }
                    
                    $items = [];
                    foreach($students as $student)
                    {
                        $student_name = SystemUsers::find($student->student_id)->name;
                        $processData = ProcessData::getResultsByStudentClassAndDateProcess($data->class_id, $data->date_process, $student->student_id);
                        $role = StudentClass::getRoleStudent($student->student_id, $data->class_id);
                        

                        $countObservavel = 0;
                        $color = '#A9A9A9';
                        $solicito_ativo = 0;
                        $description = '';

                        if($processData->observavel_s == 1)
                        {
                            $countObservavel++;
                            $description .= 'S ';
                            $solicito_ativo++;
                        }

                        if($processData->observavel_p == 1)
                        {
                            $countObservavel++;
                            $description .= 'P ';
                            $solicito_ativo++;
                        }

                        if($processData->observavel_r == 1)
                        {
                            $countObservavel++;
                            $description .= 'R ';
                            $solicito_ativo++;
                        }

                        if($solicito_ativo == 3)
                        {
                            $description = 'Colaborativo solícito';
                        }

                        $description .= " ({$role})";

                        if($countObservavel == 3)
                        {
                            $color = '#0000FF';
                        }
                        elseif($countObservavel == 2)
                        {
                            $color = '#00BFFF';
                        }
                        
                        $items[] = ['student_id' => $student->student_id, 'group_id' => $student->group_id, 'title' => $student_name, 'content' => $description, 'color' => $color];
                    }

                    foreach ($items as $item)
                    {
                        $this->kanbanGroups->addItem($item['student_id'], $item['group_id'], $item['title'], $item['content'], $item['color']);
                    }

                    $this->kanbanGroups->setItemDropAction(new TAction([$this, 'onUpdateItemDrop']));

                }
                
                TTransaction::close();

                parent::add($this->kanbanGroups);
            }
        }
        catch(Execption $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }

    public static function onUpdateItemDrop($param)
    {
        try
        {
            TTransaction::open('collaby'); // open a transaction
            
            $group_id   = $param['stage_id'];
            $student_id = $param['id'];

            //StudentGroup::where('group_id', '=', $group_id)->delete();
            $group = new Group($group_id);

            if($student_id)
            {
                $groups = StudentGroup::getGroupsByStudentClass($student_id, $group->class_id);
                if($groups)
                {
                    foreach($groups as $group)
                    {
                        if($group->group_id <> $group_id)
                        {
                            StudentGroup::where('group_id', '=', $group->group_id)
                                        ->where('student_id', '=', $student_id)->delete();
                        }
                    }
                }

                $object_student             = new StudentGroup();
                $object_student->student_id = $student_id;
                $object_student->group_id   = $group_id;

                $object_student->store();  
            } 

            TTransaction::close(); // close the transaction

            new TMessage('info', "Aluno movido de grupo!");

        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage()); // shows the exception error message
            $this->form->setData( $this->form->getData() ); // keep form data
            TTransaction::rollback(); // undo all pending operations
        }
    }
    
}
